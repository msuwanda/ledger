<?php
include $_SERVER['DOCUMENT_ROOT']."/conf/setDB01.php";
require_once __DIR__ . '/vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


$helper = new Sample();
if ($helper->isCli()) {
    $helper->log('This example should only be run from a Web Browser' . PHP_EOL);

    return;
}
// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document propertiesas
$spreadsheet->getProperties()->setCreator('Muhamamd Suwanda')
    ->setLastModifiedBy('spectra-app')
    ->setTitle('Excel buku besar')
    ->setSubject('Office 2007 XLSX Test Document')
    ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    ->setKeywords('office 2007 openxml php')
    ->setCategory('Test result file');

// Add some data
    $que 	 = "SELECT item_kode,item_barang FROM tr_item_valas WHERE kln_id = '".$_GET['kid']."'  AND item_tahun = '".$_GET['thn']."' GROUP BY item_kode" ;
    $fetch = $PLINK->query($que);
    $i = 1 ;
    $body = [
    'font' => [
        'bold' => false,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $tittle = [
    'font' => [
        'bold' => true,
    ],];
    $header = [
    'font' => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $foot = [
    'font' => [
        'bold' => true,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];


   while ($row = $fetch->fetch_array()) {
      $k = $i + 1 ;
      $c = $i + 2 ;
      $v = $i + 3 ;
      $h = $i + 5 ;
      $f = $i + 4 ;
    // tittle
    $spreadsheet->setActiveSheetIndex(0)
          ->setCellValue('A'.$i, $_GET['kn'])
          ->setCellValue('V'.$i, 'FILTER')
          ->setCellValue('A'.$k, $row['item_kode']." - ".$row['item_barang'])
          ->setCellValue('A'.$c, "KARTU STOCK - ".$_GET['thn']);
    $spreadsheet->getActiveSheet()->getStyle('A'.$i.':A'.$c)->applyFromArray($tittle);
    // header
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$v, 'TANGGAL')
        ->setCellValue('B'.$v, 'KODE TRANS')
        ->setCellValue('C'.$v, 'Valas')
        ->setCellValue('M'.$v, 'Valas')
        ->setCellValue('L'.$v, 'kurs')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$v.':U'.$v)->applyFromArray($header);
    $spreadsheet->getActiveSheet()->mergeCells('C'.$v.':K'.$v);
    $spreadsheet->getActiveSheet()->mergeCells('M'.$v.':U'.$v);
    $spreadsheet->getActiveSheet()->mergeCells('A'.$v.':A'.$h);
    $spreadsheet->getActiveSheet()->mergeCells('B'.$v.':B'.$h);
    $spreadsheet->getActiveSheet()->mergeCells('L'.$v.':L'.$h);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('C'.$f, 'Masuk')
        ->setCellValue('F'.$f, 'Keluar')
        ->setCellValue('I'.$f, 'Saldo')
        ->setCellValue('M'.$f, 'Masuk')
        ->setCellValue('P'.$f, 'Keluar')
        ->setCellValue('S'.$f, 'Saldo')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$f.':U'.$f)->applyFromArray($header);
    $spreadsheet->getActiveSheet()->mergeCells('C'.$f.':E'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('F'.$f.':H'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('I'.$f.':K'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('M'.$f.':O'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('P'.$f.':R'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('S'.$f.':U'.$f);
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('C'.$h, 'Qty')
        ->setCellValue('D'.$h, 'Satuan')
        ->setCellValue('E'.$h, 'Total')
        ->setCellValue('F'.$h, 'Qty')
        ->setCellValue('G'.$h, 'Satuan')
        ->setCellValue('H'.$h, 'Total')
        ->setCellValue('I'.$h, 'Qty')
        ->setCellValue('J'.$h, 'Satuan')
        ->setCellValue('K'.$h, 'Total')
        ->setCellValue('M'.$h, 'Qty')
        ->setCellValue('N'.$h, 'Satuan')
        ->setCellValue('O'.$h, 'Total')
        ->setCellValue('P'.$h, 'Qty')
        ->setCellValue('Q'.$h, 'Satuan')
        ->setCellValue('R'.$h, 'Total')
        ->setCellValue('S'.$h, 'Qty')
        ->setCellValue('T'.$h, 'Satuan')
        ->setCellValue('U'.$h, 'Total')
        ;
    $sd = $i + 6 ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$h.':U'.$h)->applyFromArray($header);
    $spreadsheet->getActiveSheet()->getStyle('A'.$sd.':U'.$sd)->applyFromArray($header);
    // saldo awal
          $a = $i + 7 ;
          $que2 	 = "SELECT * FROM v_item_valas WHERE item_kode = '".$row['item_kode']."' AND kln_id = '".$_GET['kid']."'  AND item_tahun = '".$_GET['thn']."' ORDER BY item_tgl ASC,type ASC" ;
          $fetch2  = $PLINK->query($que2);

      // body
          while ($row2 = $fetch2->fetch_array()) {
            $g = $a - 1 ;
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$a, $row2['item_tgl'])
                ->setCellValue('B'.$a, $row2['kode_trans'])
                ->setCellValue('C'.$a, $row2['qm'])
                ->setCellValue('D'.$a, '=IF(C'.$a.'<>0,SUM(E'.$a.'/C'.$a.'),"")')
                ->setCellValue('E'.$a, $row2['tm'])
                ->setCellValue('F'.$a, $row2['qk'])
                ->setCellValue('G'.$a, '=IF(F'.$a.'<>0,+J'.$g.',"")')
                ->setCellValue('H'.$a, '=IF(F'.$a.'<>0,SUM(F'.$a.'*G'.$a.'),0)')
                ->setCellValue('I'.$a, '=SUM(I'.$g.'+C'.$a.'-F'.$a.')')
                ->setCellValue('J'.$a, '=IF(I'.$a.'<>0,SUM(K'.$a.'/I'.$a.'),0)')
                ->setCellValue('K'.$a, '=SUM(K'.$g.'+E'.$a.'-H'.$a.')')
                ->setCellValue('L'.$a, $row2['ks_kurs'])
                ->setCellValue('M'.$a, $row2['qm'])
                ->setCellValue('N'.$a, '=IF(M'.$a.'<>0,SUM(O'.$a.'/M'.$a.'),"")')
                ->setCellValue('O'.$a, '=SUM(L'.$a.'*'.$row2['tm'].')')
                ->setCellValue('P'.$a, $row2['qk'])
                ->setCellValue('Q'.$a, '=IF(P'.$a.'<>0,+T'.$g.',"")')
                ->setCellValue('R'.$a, '=IF(P'.$a.'<>0,SUM(P'.$a.'*Q'.$a.'),0)')
                ->setCellValue('S'.$a, '=SUM(S'.$g.'+C'.$a.'-F'.$a.')')
                ->setCellValue('T'.$a, '=IF(S'.$a.'<>0,SUM(U'.$a.'/S'.$a.'),0)')
                ->setCellValue('U'.$a, '=SUM(U'.$g.'+O'.$a.'-R'.$a.')')
                ;
            $spreadsheet->getActiveSheet()->getStyle('A'.$a.':U'.$a)->applyFromArray($body);
            $a++;
          }
          $sa = $a  ;
          $sb = $a - 1  ;
          // saldo akhir
              $spreadsheet->setActiveSheetIndex(0)
                  ->setCellValue('C'.$sa, '=SUM(C'.$f.':C'.$sb.')')
                  ->setCellValue('D'.$sa, '')
                  ->setCellValue('E'.$sa, '=SUM(E'.$f.':E'.$sb.')')
                  ->setCellValue('F'.$sa, '=SUM(F'.$f.':F'.$sb.')')
                  ->setCellValue('G'.$sa, '')
                  ->setCellValue('H'.$sa, '=SUM(H'.$f.':H'.$sb.')')
                  ->setCellValue('I'.$sa, '=+I'.$sb)
                  ->setCellValue('J'.$sa, '')
                  ->setCellValue('K'.$sa, '=+K'.$sb)
                  ->setCellValue('M'.$sa, '=SUM(M'.$f.':M'.$sb.')')
                  ->setCellValue('N'.$sa, '')
                  ->setCellValue('O'.$sa, '=SUM(O'.$f.':O'.$sb.')')
                  ->setCellValue('P'.$sa, '=SUM(P'.$f.':P'.$sb.')')
                  ->setCellValue('Q'.$sa, '')
                  ->setCellValue('R'.$sa, '=SUM(R'.$f.':R'.$sb.')')
                  ->setCellValue('S'.$sa, '=+S'.$sb)
                  ->setCellValue('T'.$sa, '')
                  ->setCellValue('U'.$sa, '=+U'.$sb)
                  ;

        $spreadsheet->getActiveSheet()->getStyle('A'.$sa.':U'.$sa)->applyFromArray($foot);
      $i = $a + 2;}

// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('Ledger');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="KARTU STOCK - '.$_GET['kn'].'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');


$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
