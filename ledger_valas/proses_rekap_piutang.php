<?php
include $_SERVER['DOCUMENT_ROOT']."/conf/setDB01.php";
require_once __DIR__ . '/vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


$helper = new Sample();
if ($helper->isCli()) {
    $helper->log('This example should only be run from a Web Browser' . PHP_EOL);

    return;
}
// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('Muhamamd Suwanda')
    ->setLastModifiedBy('spectra-app')
    ->setTitle('Excel buku besar')
    ->setSubject('Office 2007 XLSX Test Document')
    ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    ->setKeywords('office 2007 openxml php')
    ->setCategory('Test result file');

// Add some data
    $bak = explode(',',$_GET['ka']) ;
    $ard = array();
    for ($i=0; $i < count($bak); $i++) {
        $ard[] = "bak_akun = '".$bak[$i]."'" ;
    }
    $bak_akun = implode(' OR ',$ard ) ;

    $que 	 = "SELECT npwp,sup_nama,sup_saldo_awal,sup_curency FROM tr_customer_valas WHERE kln_id = '".$_GET['kid']."'  AND sup_tahun = '".$_GET['thn']."' AND sup_tahun = '".$_GET['thn']."'" ;
    $fetch = $PLINK->query($que);
    $i = 1 ;
    $body = [
    'font' => [
        'bold' => false,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $tittle = [
    'font' => [
        'bold' => true,
    ],];
    $header = [
    'font' => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $foot = [
    'font' => [
        'bold' => true,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', $_GET['kn'])
        ->setCellValue('A2', 'Rekap Kartu Piutang')
    ;
    $spreadsheet->getActiveSheet()->mergeCells('A1:J1');
    $spreadsheet->getActiveSheet()->mergeCells('A2:J2');
    $hd = 4 ;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$hd, 'NPWP')
        ->setCellValue('B'.$hd, 'Customer')
        ->setCellValue('C'.$hd, 'Valas')
        ->setCellValue('D'.$hd, '')
        ->setCellValue('E'.$hd, '')
        ->setCellValue('F'.$hd, '')
        ->setCellValue('G'.$hd, 'Rupiah')
        ->setCellValue('H'.$hd, '')
        ->setCellValue('I'.$hd, '')
        ->setCellValue('J'.$hd, '')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A4:J4')->applyFromArray($header);
    $spreadsheet->getActiveSheet()->mergeCells('C4:F4');
    $spreadsheet->getActiveSheet()->mergeCells('G4:J4');
    $spreadsheet->getActiveSheet()->mergeCells('A4:A5');
    $spreadsheet->getActiveSheet()->mergeCells('B4:B5');
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A5', '')
        ->setCellValue('B5', '')
        ->setCellValue('C5', 'SALDO AWAL')
        ->setCellValue('D5', 'DEBET')
        ->setCellValue('E5', 'CREDIT')
        ->setCellValue('F5', 'SALDO')
        ->setCellValue('G5', 'SALDO AWAL')
        ->setCellValue('H5', 'DEBET')
        ->setCellValue('I5', 'KREDIT')
        ->setCellValue('J5', 'SALDO')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A5:J5')->applyFromArray($header);

  $a = 6 ;
 while ($row = $fetch->fetch_array()) {
    $que2 	 = "SELECT * FROM tm_buku_bank_valas WHERE (".$bak_akun.") AND bk_customer = '".$row['npwp']."' AND kln_id = '".$_GET['kid']."'  AND bk_tahun = '".$_GET['thn']."' ORDER BY bk_tanggal" ;
    $fetch2  = $PLINK->query($que2);

    $saldo = $row['sup_saldo_awal'] ;
    $debit = 0 ;
    $kredit = 0 ;
    $saldo_valas = $row['sup_saldo_awal'] * $row['sup_curency']  ;
    $debit_valas = 0 ;
    $kredit_valas = 0 ;

    while ($row2 = $fetch2->fetch_array()) {
      $debit = $debit + $row2['bk_kredit'] ;
      $kredit = $kredit + $row2['bk_debet'] ;
      $saldo = $saldo + $row2['bk_kredit'] - $row2['bk_debet'] ;

      $db = $row2['bk_kredit'] * $row2['bk_curency'] ;
      $kd = $row2['bk_debet'] * $row2['bk_curency'] ;

      $debit_valas = $debit_valas + $db ;
      $kredit_valas = $kredit_valas + $kd ;
      $saldo_valas = $saldo_valas + $db - $kd ;
    }
    $saldo_awal = $row['sup_saldo_awal'] * $row['sup_curency'] ;
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$a, $row['npwp'])
        ->setCellValue('B'.$a, $row['sup_nama'])
        ->setCellValue('C'.$a, $row['sup_saldo_awal'])
        ->setCellValue('D'.$a, $debit)
        ->setCellValue('E'.$a, $kredit)
        ->setCellValue('F'.$a, $saldo)
        ->setCellValue('G'.$a, $saldo_awal)
        ->setCellValue('H'.$a, $debit_valas)
        ->setCellValue('I'.$a, $kredit_valas)
        ->setCellValue('J'.$a, $saldo_valas)
        ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$a.':J'.$a)->applyFromArray($body);

$a++;
}

// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('Ledger');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Rekap Kartu Piutang - '.$_GET['kn'].'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');


$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
