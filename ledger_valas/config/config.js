var getParam = function () {
  // This function is anonymous, is executed immediately and
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}();
function getJs(param) {
  if (param != 'undefined') {
    targetUrl = "/api/v_appjs.php" ;
    jQuery.post(targetUrl, {filter: [{name: "app_id" ,value: param}]}, function(data){
    if (data.length == 0) {
      jQuery('#page-titles').addClass('hidden') ;
    }
    else {
      if (data[0].app_js == 1) {
        jQuery.getScript(data[0].app_exe + '.js') ;
      }
      else
      {

      }
    }
    }, "json");
  }


}
// fungsi form select Team User
function getSelectTeam(teamId){
	targetUrl   = "/api/setting/view_team.php";
	jQuery.post(targetUrl, {}, function(data){
		jQuery('#option-team').append('<option value="noteam"> - </option>');
		jQuery.each(data,function(i,value){
			inAttr	= "";
			if(teamId==value.team_id){
				inAttr	= "selected";
			}
			jQuery('#option-team').append('<option value="' + value.team_id + '" ' + inAttr + '>' + value.team_nama + '</option>');
		});
	}, "json");
}
// fungsi form select grup user
function getSelectGrup(grupId){
	targetUrl   = "/api/setting/view_grup.php";
	jQuery.post(targetUrl, {}, function(data){
		jQuery('#option-grup').append('<option value="500"> - </option>');
		jQuery.each(data,function(i,value){
			inAttr	= "";
			if(grupId==value.grup_id){
				inAttr	= "selected";
			}
			jQuery('#option-grup').append('<option value="' + value.grup_id + '" ' + inAttr + '>' + value.grup_nama + '</option>');
		});
	}, "json");
}
//initializing
