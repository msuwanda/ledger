<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');

include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

require_once __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$helper = new Sample();

// Return to the caller script when runs by CLI
if ($helper->isCli()) {
    return;
}

$nilai	= $_POST['data'];

$inputFileName = $_SERVER['DOCUMENT_ROOT'].'/files/dataexcel/'.$nilai[0]['value'] ;
//$helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
$spreadsheet = IOFactory::load($inputFileName);
$sheetData 	 = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
$kode   	 = $sheetData[1]['B'] ;
$rows     	 = count($sheetData) ;
$query = "INSERT INTO kmk_kurs (ks_tanggal,ks_kurs,ks_type) VALUES" ;
$ard = array();
for ($i=4; $i <= $rows; $i++) {
  $kurs = $sheetData[$i]['C'] ;
  $begin = new DateTime($sheetData[$i]['A']);
  $end = new DateTime($sheetData[$i]['B']);
  $end = $end->modify('+1 day');

  $interval = new DateInterval('P1D');
  $daterange = new DatePeriod($begin, $interval ,$end);

  foreach($daterange as $date){
      $ard[] = "('".$date->format("Y-m-d")."','".$kurs."','".$kode."')" ;
  }
}

  $query .= implode(",",$ard) ;
	$error		= "";
		try{
			$PLINK->beginTransaction();
			$PLINK->exec($query) ;
			$jumlah  = count($sheetData)-3 ;
			$pesan 	 = "Data has been saved successfully And ".$jumlah." data has been created by the system";
			$kelas	 = "success";
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$jumlah  = "0" ;
			$pesan	 = "Data gagal disimpan ".$e->getMessage()." ".$query;
			$kelas	 = "error";
			$error	 = $e->getMessage();
		}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query,"total" => $jumlah);
	echo json_encode($pesan);
?>
