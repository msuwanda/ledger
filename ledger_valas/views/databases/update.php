<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card wizard-content card-outline-info ">
            <div class="card-header">
                <h4 class="m-b-0 text-white">form update COA untuk kartu piutang dan hutang</h4>
            </div>
            <div class="card-block">
              <form onsubmit="return false;" method="post" role="form">
                    <div class="row">
														<div class="col-md-6">
														   <div class="form-group">
														        <label>Klien id</label>
														        <input type="text" class="form-control" name="kln_id" readonly>
														   </div>
														</div>
														<div class="col-md-6">
														   <div class="form-group">
														        <label>Tahun</label>
														        <input type="text" class="form-control" name="bb_tahun" readonly>
														   </div>
														</div>
														<div class="col-md-6">
														   <div class="form-group">
														        <label>Coa Penjualan</label>
														        <input type="text" class="form-control" name="bb_penjualan">
														   </div>
														</div>
														<div class="col-md-6">
														   <div class="form-group">
														        <label>Coa Piutang</label>
														        <input type="text" class="form-control" name="bb_piutang">
														   </div>
														</div>
														<div class="col-md-6">
														   <div class="form-group">
														        <label>Coa Pembelian</label>
														        <input type="text" class="form-control" name="bb_pembelian">
														   </div>
														</div>
														<div class="col-md-6">
														   <div class="form-group">
														        <label>Coa Hutang</label>
														        <input type="text" class="form-control" name="bb_hutang">
														   </div>
														</div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                               <button type="submit" id="button-update" class="btn btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button></div>
                        </div>
                    </div>
              </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12" >
        <div class="card wizard-content card-outline-info ">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Delete Data</h4>
            </div>
            <div class="card-block">
              <table class="table" id="data">
                <thead>
                  <tr>
                    <th></th>
                    <th></th>
                    <th width="50%"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Ledger</td>
                    <td><button class="btn btn-warning" onclick="hapusLed()">Hapus Data</td>
                    <td><div class="progress"><div class="progress-bar bg-success active" style="height:14px;" id="pg-led" role="progressbar"> </div>
                    </div><div id="pesan-led" style="padding:3px"></div></td>
                  </tr>
                  <tr>
                    <td>Coa</td>
                    <td><button class="btn btn-warning" onclick="hapusCoa()">Hapus Data</td>
                    <td><div class="progress"><div class="progress-bar bg-success active" style="height:14px;" id="pg-coa" role="progressbar"> </div>
                    </div><div id="pesan-coa" style="padding:3px"></div></td>
                  </tr>
                  <tr>
                    <td>Supplier</td>
                    <td><button class="btn btn-warning" onclick="hapusSup()">Hapus Data</td>
                    <td><div class="progress"><div class="progress-bar bg-success active" style="height:14px;" id="pg-sup" role="progressbar"> </div>
                    </div><div id="pesan-sup" style="padding:3px"></div></td>
                  </tr>
                  <tr>
                    <td>Customer</td>
                    <td><button class="btn btn-warning" onclick="hapusCus()">Hapus Data</td>
                    <td><div class="progress"><div class="progress-bar bg-success active" style="height:14px;" id="pg-cus" role="progressbar"> </div>
                    </div><div id="pesan-cus" style="padding:3px"></div></td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
