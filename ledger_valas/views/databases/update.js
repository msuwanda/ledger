dataBases    = JSON.parse(localStorage.databases);
jQuery('#page-titles').removeClass('hidden') ;
jQuery('#page-titles h3').append(' <span style="color:#54667a !important">' + dataBase.kln_nama + '</span>') ;

targetUrl = "/api/ledger_valas/view_databases.php" ;
jQuery.post(targetUrl, {filter: [{name: "kln_id" ,value: dataBase.kln_id},{name: "bb_tahun" ,value: dataBase.tahun}]}, function(data){
//console.log(data[0]);
jQuery("input[name='kln_id']").attr("value",data[0].kln_id) ;
jQuery("input[name='bb_tahun']").attr("value",data[0].bb_tahun) ;
jQuery("input[name='bb_penjualan']").attr("value",data[0].bb_penjualan) ;
jQuery("input[name='bb_piutang']").attr("value",data[0].bb_piutang) ;
jQuery("input[name='bb_pembelian']").attr("value",data[0].bb_pembelian) ;
jQuery("input[name='bb_hutang']").attr("value",data[0].bb_hutang) ;
jQuery("input[name='bb_valas']").attr("value",data[0].bb_valas) ;


}, "json");

jQuery('#button-update').click(function(){
	  dataFeed 			= {filter: [{name: "kln_id" ,value: dataBase.kln_id},{name: "bb_tahun" ,value: dataBase.tahun}],data: jQuery(':input').serializeArray()};
	  console.log(dataFeed) ;
	  if(dataFeed.data[1].value.length>0){
	    targetUrl	= "/api/ledger_valas/sunting_databases.php";
	    jQuery.post(targetUrl, dataFeed, function(data){
	        //console.log(data.pesan + data.query);
	        swal({
	            title: data.title ,
	            text: data.pesan + "<br>" + data.error,
	            type: data.kelas,
	            html: true
		      }, function(){
		          window.location = "index.php?pages=119120" ;
		      });
	    }, "json");
	  }
});
function hapusCoa(){
  //jQuery('#td-' + getid).append(param) ; pg-bar
	getid = 1 ;
  jQuery('#pesan-coa').html('<img src="Flickr-1s-200px.gif" width="20px;"> checking row') ;
  id = setInterval(function(){ frame(getid)  }, 30);
  width = 10 ;
  function frame(getid){
    if (width >= 50) {
      clearInterval(id);

      console.log(width);

      jQuery('#pesan-coa').html('<img src="Flickr-1s-200px.gif" width="20px;"> execute excel') ;
      targetUrl	= "/api/ledger_valas/hapus_coa.php";
      dataFeed 	= {filter: [{name: "kln_id" ,value: dataBase.kln_id},{name: "coa_tahun" ,value: dataBase.tahun}]};
      jQuery.post(targetUrl, dataFeed ,function(data){
        //console.log(data);
        jQuery('#pesan-coa').html('<img src="Flickr-1s-200px.gif" width="20px;"> delete record') ;
        id = setInterval(function(){ frame(getid)  }, 30);
        width = 50 ;
        function frame(getid){
          if (width >= 100) {
            clearInterval(id) ;
            jQuery('#pesan-coa').html('<i class="fa fa-circle" style="font-size:13px;margin-right:5px"></i> ' + data.pesan) ;
          } else {
            width++;
            jQuery('#pg-coa').attr("style","width :" + width + "%");
            jQuery('#pg-coa').html( width + "%" );
            //console.log(width);
          }
        }

      }, "json");

    } else {
      width++;
      jQuery('#pg-coa').attr("style","width :" + width + "%");
      jQuery('#pg-coa').html( width + "%" );
      console.log(width);
    }
  }

}
function hapusLed(){
  //jQuery('#td-' + getid).append(param) ; pg-bar
	getid = 4 ;
  jQuery('#pesan-led').html('<img src="Flickr-1s-200px.gif" width="20px;"> checking row') ;
  id = setInterval(function(){ frame(getid)  }, 30);
  width = 10 ;
  function frame(getid){
    if (width >= 50) {
      clearInterval(id);

      console.log(width);

      jQuery('#pesan-led').html('<img src="Flickr-1s-200px.gif" width="20px;"> execute excel') ;
      targetUrl	= "/api/ledger_valas/hapus_led.php";
      dataFeed 	= {filter: [{name: "kln_id" ,value: dataBase.kln_id},{name: "bk_tahun" ,value: dataBase.tahun}]};
      jQuery.post(targetUrl, dataFeed ,function(data){
        //console.log(data);
        jQuery('#pesan-led').html('<img src="Flickr-1s-200px.gif" width="20px;"> delete record') ;
        id = setInterval(function(){ frame(getid)  }, 30);
        width = 50 ;
        function frame(getid){
          if (width >= 100) {
            clearInterval(id) ;
            jQuery('#pesan-led').html('<i class="fa fa-circle" style="font-size:13px;margin-right:5px"></i>' + data.pesan) ;
          } else {
            width++;
            jQuery('#pg-led').attr("style","width :" + width + "%");
            jQuery('#pg-led').html( width + "%" );
            //console.log(width);
          }
        }

      }, "json");

    } else {
      width++;
      jQuery('#pg-led').attr("style","width :" + width + "%");
      jQuery('#pg-led').html( width + "%" );
      console.log(width);
    }
  }

}


function hapusSup(){
  //jQuery('#td-' + getid).append(param) ; pg-bar
	getid = 2 ;
  jQuery('#pesan-sup').html('<img src="Flickr-1s-200px.gif" width="20px;"> checking row') ;
  id = setInterval(function(){ frame(getid)  }, 30);
  width = 10 ;
  function frame(getid){
    if (width >= 50) {
      clearInterval(id);

      console.log(width);

      jQuery('#pesan-sup').html('<img src="Flickr-1s-200px.gif" width="20px;"> execute excel') ;
      targetUrl	= "/api/ledger_valas/hapus_sup.php";
      dataFeed 	= {filter: [{name: "kln_id" ,value: dataBase.kln_id},{name: "sup_tahun" ,value: dataBase.tahun}]};
      jQuery.post(targetUrl, dataFeed ,function(data){
        //console.log(data);
        jQuery('#pesan-sup').html('<img src="Flickr-1s-200px.gif" width="20px;"> delete record') ;
        id = setInterval(function(){ frame(getid)  }, 30);
        width = 50 ;
        function frame(getid){
          if (width >= 100) {
            clearInterval(id) ;
            jQuery('#pesan-sup').html('<i class="fa fa-circle" style="font-size:13px;margin-right:5px"></i> ' + data.pesan) ;
          } else {
            width++;
            jQuery('#pg-sup').attr("style","width :" + width + "%");
            jQuery('#pg-sup').html( width + "%" );
            //console.log(width);
          }
        }

      }, "json");

    } else {
      width++;
      jQuery('#pg-sup').attr("style","width :" + width + "%");
      jQuery('#pg-sup').html( width + "%" );
      console.log(width);
    }
  }

}
function hapusCus(){
  //jQuery('#td-' + getid).append(param) ; pg-bar
	getid = 2 ;
  jQuery('#pesan-cus').html('<img src="Flickr-1s-200px.gif" width="20px;"> checking row') ;
  id = setInterval(function(){ frame(getid)  }, 30);
  width = 10 ;
  function frame(getid){
    if (width >= 50) {
      clearInterval(id);

      console.log(width);

      jQuery('#pesan-cus').html('<img src="Flickr-1s-200px.gif" width="20px;"> execute excel') ;
      targetUrl	= "/api/ledger_valas/hapus_cus.php";
      dataFeed 	= {filter: [{name: "kln_id" ,value: dataBase.kln_id},{name: "sup_tahun" ,value: dataBase.tahun}]};
      jQuery.post(targetUrl, dataFeed ,function(data){
        //console.log(data);
        jQuery('#pesan-cus').html('<img src="Flickr-1s-200px.gif" width="20px;"> delete record') ;
        id = setInterval(function(){ frame(getid)  }, 30);
        width = 50 ;
        function frame(getid){
          if (width >= 100) {
            clearInterval(id) ;
            jQuery('#pesan-cus').html('<i class="fa fa-circle" style="font-size:13px;margin-right:5px"></i> ' + data.pesan) ;
          } else {
            width++;
            jQuery('#pg-cus').attr("style","width :" + width + "%");
            jQuery('#pg-cus').html( width + "%" );
            //console.log(width);
          }
        }

      }, "json");

    } else {
      width++;
      jQuery('#pg-cus').attr("style","width :" + width + "%");
      jQuery('#pg-cus').html( width + "%" );
      console.log(width);
    }
  }

}
