<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<style>
  table {
     font-size: 14px !important ;
     line-height: 1;
  }
  @media print {
    .left-sidebar {
      display: none;
    }
    .topbar {
      display: none;
    }
    .page-wrapper {
      padding-top: 0px;
    }
  }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
              <h2 id="kln"> PT. Surya Usaha Mandiri</h2>
              <h4 id="akun"> 1114.03.00</h4>
              <h5 id="tahun"> 1114.03.00</h5>

              <table id="datatable" class="table table-hover table-bordered table-striped dataTable">
                  <thead>
                      <tr>
                          <th width="15%" >Tanggal</th>
                          <th>No. Bukti</th>
                          <th>Keterangan</th>
                          <th>Debet</th>
                          <th>Kredit</th>
                          <th>Saldo</th>
                          <th>Sumber Trans</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>

                      </tfoot>
               </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
