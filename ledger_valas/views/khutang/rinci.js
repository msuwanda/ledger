dataBases    = JSON.parse(localStorage.databases);
jQuery('#kln').html(dataBases.kln_nama) ;
jQuery('#akun').html(getParam.akun + ' - ' + getParam.nakun) ;
jQuery('#tahun').html("BUKU BESAR - " + dataBases.tahun) ;
document.title = getParam.akun + ' - ' + getParam.nakun ;
dataBases    = JSON.parse(localStorage.databases);

$(document).ready(function() {
	//kunci    = JSON.parse(localStorage.karyawan);
	var dt = $('#datatable').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
    "ordering": false,
		"sAjaxSource": "/api/ledger_valas/view_kartu_hutang.php?c=" + getParam.akun + "&t=" + dataBases.tahun + "&k=" + dataBases.kln_id + "&p1=" + dataBases.idpem + "&p2=" + dataBases.idhut + "&s=" + getParam.s + "&sk=" + getParam.c ,
    "sSearch": false,
		"searching": false ,
		"paging":   false,
		dom: 'Bfrtip',
		buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
		],
    "columns":[
            {
						"class":          "details-control",
						"orderable":      false,
						"data":           "0",
						"defaultContent": ""
					 },
            { "data": "1","orderable": false },
            { "data": "2","orderable": false },
            { "data": "4","orderable": false },
            { "data": "3","orderable": false },
            { "data": "8","orderable": false  },
						{ "data": "6","orderable": false  },
						{ "data": "9","orderable": false  },
						{ "data": "10","orderable": false  },
						{ "data": "11","orderable": false  },
            { "data": "5","orderable": false },
           ]
	} );
/*
	var detailRows = [];

	$('#datatable tbody').on( 'click', 'tr td.details-control', function () {
			var data = dt.row( $(this).parents('tr') ).data();
			window.location.href = "index.php?pages=401020&id="+ data[8] +"";
	} );
*/
	$('#datatable').attr('style', 'width:100%;');
} );

targetUrl = "/api/ledger/view_akun.php" ;

jQuery.post(targetUrl, {filter : [{name: "bk_tahun" ,value: dataBases.tahun},{name: "kln_id" ,value: dataBases.kln_id},{name: "bak_akun" ,value: getParam.akun}]}, function(data){
  jQuery.each(data,function(i,value){

    // defining kontrol proses
    jQuery('#datatable tfoot').html('<tr><th colspan="3">Total</th><th>' + value.bak_debet + '</th><th>' + value.bak_kredit + '</th><th>' + value.bak_total + '</th><th></th></tr>');

  });
}, "json");
