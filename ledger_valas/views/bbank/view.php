<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<style>
  table {
     font-size: 14px !important ;
     line-height: 1;
  }
  @media print {
    .left-sidebar {
      display: none;
    }
    .topbar {
      display: none;
    }
    .page-wrapper {
      padding-top: 0px;
    }
  }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
              <table id="datatable" class="table table-hover table-bordered table-striped dataTable">
                  <thead>
                      <tr>
                          <th width="15%">Tanggal</th>
                          <th>Kode Akun</th>
                          <th>Nama Akun</th>
                          <th>No. Bukti</th>
                          <th>Keterangan</th>
                          <th>Debet</th>
                          <th>Kredit</th>
                          <th>Sumber Trans</th>
                          <th>Akun Lawan</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
               </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
