<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
  <div class="col-12">
  <?php //Z:\LAIN-LAIN\IT\1. FORMAT UPLOAD ?>
    <div class="ribbon-wrapper card">
        <div class="ribbon ribbon-success">FORMAT UPLOAD</div>
        <p class="ribbon-content">lokasi format upload W:\LAIN-LAIN\IT\1. FORMAT UPLOAD </p>
    </div>
  </div>
    <div class="col-6">
        <div class="card card-outline-primary">
          <div class="card-header ">
                <h4 class="m-b-0 text-white">List Databases <small class="text-muted">Silahkan pilih databases</small></h4>
            </div>
            <div class="card-block">
                <table class="table table-hover table-condensed dataTable no-footer" id="tableuser">
                  <thead>
                    <tr>
                			<th>Klien</th>
                			<th>Tahun</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-6">
        <div class="card card-outline-primary">
          <div class="card-header ">
                <h4 class="m-b-0 text-white">Tambah Databases<small class="text-muted"></small></h4>
            </div>
            <div class="card-block">
              <form onsubmit="return false;" method="post" role="form">
		            <div class="row">
                  <div class="col-md-4">
  			            <div class="form-group">
  			              <label>Klien</label>
  			                 <input type='text' name='kln_id' class='form-control' id="klien_id">
  			            </div>
  			          </div>
                  <div class="col-md-8">
  			            <div class="form-group">
  			              <label>Klien</label>
  			                 <input type='text' name='kln_nama' class='form-control' id="klien_nama" readonly>
  			            </div>
  			          </div>
  			          <div class="col-md-12">
  			            <div class="form-group">
                      <label >Tahun</label>
                      <select name='masa_tahun' class='form-control'>
                        <option value="0">-</option>
                        <?php for ($i=2014; $i <= 2020; $i++) {
                        ?>
                          <option value="<?= $i ?>"><?= $i ?></option>
                        <?php
                        }
                        ?>
                      </select>
  			            </div>
  			          </div>
                  <div class="col-md-8">
  			            <div class="form-group">
  			              <label>COA Kartu Piutang <small> Pisah dengan koma "," </small></label><br>
                      <input name="bb_penjualan" type="text" value="" class='form-control' data-role="tagsinput" placeholder="" id="data" />
  			            </div>
  			          </div>
                  <div class="col-md-6 hidden">
  			            <div class="form-group">
  			              <label>.</label>
  			                 <input type='text' name='bb_piutang' class='form-control' placeholder="kode piutang">
  			            </div>
  			          </div>
                  <div class="col-md-8">
  			            <div class="form-group">
  			              <label>COA Kartu hutang <small> Pisah dengan koma "," </small></label>
                         <input name="bb_pembelian" type="text" value="" class='form-control' data-role="tagsinput" placeholder="" id="data" />
  			            </div>
  			          </div>
                  <div class="col-md-6 hidden">
  			            <div class="form-group">
  			              <label>.</label>
  			                 <input type='text' name='bb_hutang' class='form-control' placeholder="kode hutang">
  			            </div>
  			          </div>
			        </div>
			      	<div class="row">
			          <div class="col-md-12">
			            <div class="form-group">
			              <div>
			                <button type="submit" id="button-simpan" class="btn btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
			              </div>
			            </div>
			          </div>
			        </div>
		        </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
