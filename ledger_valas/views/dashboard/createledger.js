jQuery('#page-titles').removeClass('hidden') ;

  $("#klien_id").autocomplete({
    source: '/api/item-data.php',
    minLength: 1,
    select: function(event, ui) {
        var $itemrow = $(this).closest('tr');
                // Populate the input fields from the returned values
                $('#klien_id').val(ui.item.itemCode);
                $('#klien_nama').val(ui.item.itemDesc);
        return false;
      }
  }).data("ui-autocomplete")._renderItem = function( ul, item ) {
      return $( "<li></li>" )
          .data( "item.autocomplete", item )
          .append( "<a>" + item.itemCode + " - " + item.itemDesc + "</a>" )
          .appendTo( ul );
  };

  jQuery('#button-simpan').click(function(){
    dataFeed 			= {data: jQuery(':input').serializeArray()};


    if(dataFeed.data[0].value.length>0 && dataFeed.data[2].value.length>0){
        console.log(dataFeed);
      //jQuery('button').prop('disabled', true);
      targetUrl	= "/api/ledger/tambah_buku.php";
      jQuery.post(targetUrl, dataFeed, function(data){
        console.log( data.pesan + data.query);
        swal({
            title: data.title ,
            text: data.pesan + "<br>" + data.error,
            type: data.kelas,
            html: true
        }, function(){
            window.location = "index.php?pages=" + data.url;
        });
      }, "json");
    }
  });
