<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<style>
  table {
     font-size: 14px !important ;
     line-height: 1;
  }
  @media print {
    .left-sidebar {
      display: none;
    }
    .topbar {
      display: none;
    }
    .page-wrapper {
      padding-top: 0px;
    }
  }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
              <h2 id="kln"> PT. Surya Usaha Mandiri</h2>
              <h4 id="tahun"> 1114.03.00</h4>
                <table class="table table-hover table-bordered table-striped dataTable" id="tableuser">
                  <thead>
                    <tr>
                			<th>Kode akun</th>
                			<th>Nama Akun</th>
                      <th>Debit</th>
                      <th>Kredit</th>
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
