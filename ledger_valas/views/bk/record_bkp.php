<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <table class="table table-condensed dataTable no-footer" id="tableuser">
                  <thead>
                    <tr>
                			<th>File</th>
                			<th></th>
                			<th width="50%"></th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                          error_reporting(E_ALL | E_STRICT);
                          ini_set('display_errors', 'On');
                          include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";
                          $query = "SELECT tmp_id,tmp_nama,tmp_status,tmp_file FROM tmp_file_bk WHERE tmp_session = :tmp" ;
                          $stmt = $PLINK->prepare($query);
                          $stmt->execute(['tmp' => $_GET['id']]);
                          while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                      ?>
                      <tr>
                        <td><?= $row['tmp_nama'] ; ?></td>
                        <td><button id="btn-<?= $row['tmp_id'] ; ?>" onclick="loadFile('<?= $row['tmp_file'] ; ?>','<?= $row['tmp_id'] ; ?>')" class="btn">Proses</button></td>
                        <td id="td-<?= $row['tmp_id'] ; ?>"><div class="progress">
                                            <div class="progress-bar bg-success active" style="height:14px;" id="pg-<?= $row['tmp_id'] ; ?>" role="progressbar"> </div>
                                        </div><div id="txt-<?= $row['tmp_id'] ; ?>" style="padding:3px"></div></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td>Konversi kurs</td>
                        <td><button id="btn-100" onclick="loadKonversi('100')" class="btn">Proses</button></td>
                        <td id="td-100">
                          <div class="progress">
                                  <div class="progress-bar bg-success active" style="height:14px;" id="pg-100" role="progressbar"> </div>
                                </div><div id="txt-100" style="padding:3px"></div>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
                <div class="row">
                  <div class="col-md-12">
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
