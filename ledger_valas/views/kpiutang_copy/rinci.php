<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<style>
  table {
     font-size: 14px !important ;
     line-height: 1;
  }
  @media print {
    .left-sidebar {
      display: none;
    }
    .topbar {
      display: none;
    }
    .page-wrapper {
      padding-top: 0px;
    }
  }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
              <h4 id="akun"> 1114.03.00</h4>

              <table id="datatable" class="table table-hover table-bordered table-striped dataTable">
                  <thead>
                      <tr>
                          <th colspan="3"></th>
                          <th colspan="3">VALAS</th>
                          <th></th>
                          <th colspan="3">RUPIAH</th>
                          <th></th>
                      </tr>
                      <tr>
                          <th width="15%">Tanggal</th>
                          <th>No. Bukti</th>
                          <th>Keterangan</th>
                          <th>Debet</th>
                          <th>Kredit</th>
                          <th>Saldo</th>
                          <th>kurs</th>
                          <th>Debet</th>
                          <th>Kredit</th>
                          <th>Saldo</th>
                          <th>Sumber Trans</th>
                      </tr>
                      </thead>
                      <tbody>
                      </tbody>
               </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
