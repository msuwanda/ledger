<?php include_once 'config/config.php'; $pages = new Pages($_GET['pages'],'111') ;  ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title><?= $pages->_getTittle ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/plugins/jquery/jquery-ui.css" rel="stylesheet">
    <link href="../assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="../assets/plugins/datatables/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/purple.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="fix-header card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="../">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="../assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="../assets/images/logo-accounting-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!-- End Logo icon -->
                        <!-- Logo text -->
                        <span>
                         <!-- dark Logo text -->
                         <img src="../assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->
                         <img src="../assets/images/logo-accounting-text-valaspng.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                      <li class="nav-item"><a class="nav-link" id="nav-data">Pilih dataBase</a></li>
                      <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="../assets/images/users/1.jpg" id="img" alt="user" class="profile-pic" /></a>
                          <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <ul class="dropdown-user">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img"><img  id="img1" src="../assets/images/users/1.jpg" alt="user"></div>
                                        <div class="u-text">
                                            <h5><?php $nm=explode(" ",$_SESSION['Name_c']);?><?=$nm[0];?></h5>
                                            <p class="text-muted"><?=$_SESSION['User_n'];?></p><a href="../profile/index.php?pages=701000" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                    </div>
                                </li>

                                <li role="separator" class="divider"></li>
                                <li><a href="../logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                          </div>
                      </li>

                        <li class="nav-item"> <a class="nav-link right-side-toggle text-muted waves-effect waves-dark"><i class="ti-menu"></i></a> </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">

                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="in">

                        <li class="nav-small-cap">PERSONAL</li>
                        <li class=""><a href="index.php?pages=111000" aria-expanded="false"><i class="fa fa-circle"></i><span class="hide-menu">Home</span></a></li>
                        <!--<li class=""><a href="index.php?pages=112000" aria-expanded="false"><i class="fa fa-circle"></i><span class="hide-menu">COA</span></a></li>-->
                        <li class=""><a href="index.php?pages=114000" aria-expanded="false"><i class="fa fa-circle"></i><span class="hide-menu">Buku Besar</span></a></li>
                        <li><a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Upload Format</span></a>
                            <ul aria-expanded="false" class="collapse">
                              <li class=""><a href="index.php?pages=117040" aria-expanded="false">KAS / BANK</span></a></li>
                              <li class=""><a href="index.php?pages=117050" aria-expanded="false">Buku Penjualan</span></a></li>
                              <li class=""><a href="index.php?pages=117060" aria-expanded="false">Buku Pembelian</span></a></li>
                              <li class=""><a href="index.php?pages=117000" aria-expanded="false">Jurnal Umum</span></a></li>
                              <li class=""><a href="index.php?pages=117020" aria-expanded="false">Supplier / customer & Saldo Awal</span></a></li>
                              <li class=""><a href="index.php?pages=117030" aria-expanded="false">COA & Saldo Awal</span></a></li>
                              <li class=""><a href="index.php?pages=117070" aria-expanded="false">Penjualan Barang/item</span></a></li>
                              <li class=""><a href="index.php?pages=117080" aria-expanded="false">Pembelian Barang/item</span></a></li>
                              <li class=""><a href="index.php?pages=117090" aria-expanded="false">KMK</span></a></li>

                            </ul>
                        </li>
                        <li><a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Kartu</span></a>
                            <ul aria-expanded="false" class="collapse">
                              <li class=""><a href="index.php?pages=115000" aria-expanded="false">Kartu Piutang</span></a></li>
                              <li class=""><a href="index.php?pages=118000" aria-expanded="false">Kartu Hutang</span></a></li>
                              <li class=""><a href="index.php?pages=119000" aria-expanded="false">Kartu Stok</span></a></li>
                            </ul>
                        </li>
                        <li><a class="has-arrow " href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Referensi</span></a>
                            <ul aria-expanded="false" class="collapse">
                              <li class=""><a href="index.php?pages=113000" aria-expanded="false">Buku Bank</span></a></li>
                              <li class=""><a href="index.php?pages=111100" aria-expanded="false">Supplier</span></a></li>
                              <li class=""><a href="index.php?pages=111200" aria-expanded="false">Customer</span></a></li>
                              <li class=""><a href="index.php?pages=111300" aria-expanded="false">Chart Of Account</span></a></li>
                              <li class=""><a href="index.php?pages=111210" aria-expanded="false">KMK</span></a></li>
                            </ul>
                        </li>
                        <li class=""><a href="index.php?pages=119120" aria-expanded="false"><i class="fa fa-circle"></i><span class="hide-menu">Setting</span></a></li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->

        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->

            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles hidden" id="page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?= $pages->_app_nama ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)"><?= $pages->_getTittle ?></a></li>
                            <li class="breadcrumb-item active" id="sub_bread"><?= $pages->_app_nama ?></li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center" id="buttonoption">
                        <a href="index.php?pages=<?= substr($pages->_parameter_pages,0,4) ?>10" class="btn pull-right hidden-sm-down btn-success hidden" id="btnCte"><i class="mdi mdi-plus-circle"></i> Create</a>

                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- Content -->
                <?php $pages->getContet() ?>
                <!-- ============================================================== -->
                <!-- End Content  -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> All APP <span><i class="btn ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">

                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © 2017 Monster Admin by wrappixel.com
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <script src="../assets/plugins/jquery/jquery-ui.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../assets/plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="../assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="../assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="../assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="../assets/plugins/datatables/jszip.min.js"></script>

    <script src="../assets/plugins/datatables/buttons.flash.min.js"></script>
    <script src="../assets/plugins/datatables/buttons.html5.min.js"></script>

    <script src="../assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <!-- ============================================================== -->
    <!-- Config JS -->
    <script src="config/config.js"></script>
    <script type="text/javascript">
        getJs(getParam.pages) ;
        dataBase2    = JSON.parse(localStorage.databases);
        jQuery("#nav-data").html(dataBase2.kln_nama + ' ' + dataBase2.tahun) ;
    </script>
    <!-- ============================================================== -->
</body>

</html>
