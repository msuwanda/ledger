<?php
include $_SERVER['DOCUMENT_ROOT']."/conf/setDB01.php";
require_once __DIR__ . '/vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


$helper = new Sample();
if ($helper->isCli()) {
    $helper->log('This example should only be run from a Web Browser' . PHP_EOL);

    return;
}
// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('Muhamamd Suwanda')
    ->setLastModifiedBy('spectra-app')
    ->setTitle('Excel buku besar')
    ->setSubject('Office 2007 XLSX Test Document')
    ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    ->setKeywords('office 2007 openxml php')
    ->setCategory('Test result file');

// Add some data
    $bak = explode(',',$_GET['ka']) ;
    $ard = array();
    for ($i=0; $i < count($bak); $i++) {
        $ard[] = "bak_akun = '".$bak[$i]."'" ;
    }
    $bak_akun = implode(' OR ',$ard ) ;

    $que 	 = "SELECT npwp,sup_nama,sup_saldo_awal,sup_curency FROM tr_customer_valas WHERE kln_id = '".$_GET['kid']."'  AND sup_tahun = '".$_GET['thn']."' AND sup_tahun = '".$_GET['thn']."'" ;
    $fetch = $PLINK->query($que);
    $i = 1 ;
    $body = [
    'font' => [
        'bold' => false,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $tittle = [
    'font' => [
        'bold' => true,
    ],];
    $header = [
    'font' => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $foot = [
    'font' => [
        'bold' => true,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];


   while ($row = $fetch->fetch_array()) {
      $k = $i + 1 ;
      $c = $i + 2 ;
      $h = $i + 5 ;
      $zx = $i + 4 ;
      $f = $i + 6 ;
    // tittle
    $spreadsheet->setActiveSheetIndex(0)
          ->setCellValue('A'.$i, $_GET['kn'])
          ->setCellValue('L'.$i, 'FILTER')
          ->setCellValue('A'.$k, $row['npwp']." - ".$row['sup_nama'])
          ->setCellValue('A'.$c, "KARTU PIUTANG - ".$_GET['thn']);
    $spreadsheet->getActiveSheet()->getStyle('A'.$i.':A'.$c)->applyFromArray($tittle);
    // header
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$zx, '')
        ->setCellValue('B'.$zx, '')
        ->setCellValue('C'.$zx, '')
        ->setCellValue('D'.$zx, 'VALAS')
        ->setCellValue('E'.$zx, '')
        ->setCellValue('F'.$zx, '')
        ->setCellValue('G'.$zx, '')
        ->setCellValue('H'.$zx, 'RUPIAH')
        ->setCellValue('I'.$zx, '')
        ->setCellValue('J'.$zx, '')
        ->setCellValue('K'.$zx, '')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$zx.':K'.$zx)->applyFromArray($header);
    $spreadsheet->getActiveSheet()->mergeCells('D'.$zx.':F'.$zx);
    $spreadsheet->getActiveSheet()->mergeCells('H'.$zx.':J'.$zx);
    $spreadsheet->getActiveSheet()->mergeCells('A'.$zx.':C'.$zx);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$h, 'TANGGAL')
        ->setCellValue('B'.$h, 'NO. BUKTI')
        ->setCellValue('C'.$h, 'KETERANGAN')
        ->setCellValue('D'.$h, 'DEBET')
        ->setCellValue('E'.$h, 'CREDIT')
        ->setCellValue('F'.$h, 'SALDO')
        ->setCellValue('G'.$h, 'KURS')
        ->setCellValue('H'.$h, 'DEBET')
        ->setCellValue('I'.$h, 'KREDIT')
        ->setCellValue('J'.$h, 'SALDO')
        ->setCellValue('K'.$h, 'KET')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$h.':K'.$h)->applyFromArray($header);
    // saldo awal
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$f, '')
        ->setCellValue('B'.$f, '')
        ->setCellValue('C'.$f, 'SALDO')
        ->setCellValue('D'.$f, '')
        ->setCellValue('E'.$f, '')
        ->setCellValue('F'.$f, $row['sup_saldo_awal'])
        ->setCellValue('G'.$f, $row['sup_curency'])
        ->setCellValue('H'.$f, '')
        ->setCellValue('I'.$f, '')
        ->setCellValue('J'.$f, '=SUM(F'. $f .' * G'. $f .')')
        ->setCellValue('K'.$f, 'SALDO AWAL')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$f.':K'.$f)->applyFromArray($body);

          $a = $i + 7  ;
          $que2 	 = "SELECT * FROM tm_buku_bank_valas WHERE (".$bak_akun.") AND bk_customer = '".$row['npwp']."' AND kln_id = '".$_GET['kid']."'  AND bk_tahun = '".$_GET['thn']."' ORDER BY bk_tanggal" ;
          $fetch2  = $PLINK->query($que2);

      // body
          while ($row2 = $fetch2->fetch_array()) {

              $debet  = $row2['bk_debet'] ;
              $kredit = $row2['bk_kredit']  ;


            $g = $a - 1 ;
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$a, $row2['bk_tanggal'])
                ->setCellValue('B'.$a, $row2['bk_bukti'])
                ->setCellValue('C'.$a, $row2['bk_keterangan'])
                ->setCellValue('D'.$a, $row2['bk_kredit'])
                ->setCellValue('E'.$a, $row2['bk_debet'])
                ->setCellValue('F'.$a, '=SUM(F'. $g .' + D'. $a .' - E'. $a .')')
                ->setCellValue('G'.$a, $row2['bk_curency'])
                ->setCellValue('H'.$a, '=SUM(G'. $a .' * D'. $a .')')
                ->setCellValue('I'.$a, '=SUM(G'. $a .' * E'. $a .')')
                ->setCellValue('J'.$a, '=SUM(J'. $g .' + H'. $a .' - I'. $a .')')
                ->setCellValue('K'.$a, $row2['bk_smb_data'])
                ;
            $spreadsheet->getActiveSheet()->getStyle('A'.$a.':K'.$a)->applyFromArray($body);
            $a++;
          }
          $sa = $a  ;
          $sb = $a - 1  ;
          // saldo akhir
          $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue('A'.$sa, '')
              ->setCellValue('B'.$sa, '')
              ->setCellValue('C'.$sa, 'TOTAL')
              ->setCellValue('D'.$sa, '=SUM(D'.$f.':D'.$sb.')')
              ->setCellValue('E'.$sa, '=SUM(E'.$f.':E'.$sb.')')
              ->setCellValue('F'.$sa, '=SUM(F'.$f.' + D'.$sa.' - E'.$sa.')')
              ->setCellValue('G'.$sa, '')
              ->setCellValue('H'.$sa, '=SUM(H'.$f.':H'.$sb.')')
              ->setCellValue('I'.$sa, '=SUM(I'.$f.':I'.$sb.')')
              ->setCellValue('J'.$sa, '=SUM(J'.$f.' + H'.$sa.' - I'.$sa.')')
              ;
        $spreadsheet->getActiveSheet()->getStyle('A'.$sa.':K'.$sa)->applyFromArray($foot);
      $i = $a + 2;}

// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('Ledger');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$_GET['kn'].'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');


$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
