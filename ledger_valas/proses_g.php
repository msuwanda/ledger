<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');

include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

require_once __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

error_reporting(E_ALL);

$helper = new Sample();

// Return to the caller script when runs by CLI
if ($helper->isCli()) {
    return;
}

$nilai	= $_POST['data'];

$inputFileName = $_SERVER['DOCUMENT_ROOT'].'/files/dataexcel/'.$nilai[0]['value'] ;
//$helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
$spreadsheet = IOFactory::load($inputFileName);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
$kln_id   = $sheetData[1]['B'] ;
$smbdata  = $sheetData[2]['B'] ;
$coalawan = $sheetData[3]['B'] ;
$tahun    = $sheetData[4]['B'] ;
$rows    =count($sheetData) ;
$query = "INSERT INTO tm_buku_bank_valas (bak_akun,bk_bukti,bk_tanggal,bk_tahun,bk_nm_akun,bk_keterangan,bk_debet,bk_kredit,bk_smb_data,kln_id,bk_customer,coa_lawan,bk_curency) VALUES" ;
for ($i=7; $i <= $rows; $i++) {
  $query .= "('".$sheetData[$i]['C']."','".$sheetData[$i]['B']."','".$sheetData[$i]['A']."','$tahun','".$sheetData[$i]['D']."','".$sheetData[$i]['E']."','".$sheetData[$i]['G']."','".$sheetData[$i]['F']."','$smbdata','$kln_id','".$sheetData[$i]['H']."','$coalawan','".$sheetData[$i]['I']."')," ;
  $query .= "('".$coalawan."','".$sheetData[$i]['B']."','".$sheetData[$i]['A']."','$tahun','".$smbdata."','".$sheetData[$i]['E']."','".$sheetData[$i]['F']."','".$sheetData[$i]['G']."','$smbdata','$kln_id','','','".$sheetData[$i]['I']."')" ;
  if(($i+1)<($rows + 1)){
    $query	.= ",";
  }
}
//$query .= ",".$query2
//echo $query ;



	$error		= "";
		try{
			$PLINK->beginTransaction();
			$PLINK->exec($query) ;
			$jumlah  = count($sheetData)-6 ;
			$pesan 	 = "Data has been saved successfully And ".$jumlah." data has been created by the system";
			$kelas	 = "success";
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$jumlah  = "0" ;
			$pesan	 = "Data gagal disimpan ".$e->getMessage()." <br> ".$query." <br>";
			$kelas	 = "error";
			$error	 = $e->getMessage();
		}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query,"total" => $jumlah);
	echo json_encode($pesan);
?>
