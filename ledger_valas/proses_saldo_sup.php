<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');

include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

require_once __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

error_reporting(E_ALL);

$helper = new Sample();

// Return to the caller script when runs by CLI
if ($helper->isCli()) {
    return;
}

$nilai	= $_POST['data'];

$inputFileName = $_SERVER['DOCUMENT_ROOT'].'/files/dataexcel/'.$nilai[0]['value'] ;
//$helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
$spreadsheet = IOFactory::load($inputFileName);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
$kln_id  = $sheetData[1]['B'] ;
$tahun   = $sheetData[2]['B'] ;
$kode    = $sheetData[3]['B'] ;
$rows    =count($sheetData) ;
$query = "INSERT INTO tr_".$kode."_valas (npwp,sup_nama,sup_saldo_awal,kln_id,sup_tahun,sup_curency) VALUES" ;
for ($i=6; $i <= $rows; $i++) {
  $query .= "('".$sheetData[$i]['A']."','".$sheetData[$i]['B']."','".$sheetData[$i]['C']."','".$kln_id."','".$tahun."','".$sheetData[$i]['D']."')" ;
  if(($i+1)<($rows + 1)){
    $query	.= ",";
  }
}
//echo $query ;



	$error		= "";
		try{
			$PLINK->beginTransaction();
			$PLINK->exec($query) ;
			$jumlah  = count($sheetData) - 5 ;
			$pesan 	 = "Data telah berhasil disimpan sup cus";
			$kelas	 = "success";
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$jumlah  = "0" ;
			$pesan	 = "Data gagal disimpan";
			$kelas	 = "error";
			$error	 = $e->getMessage();
		}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query,"total" => $jumlah);
	echo json_encode($pesan);
?>
