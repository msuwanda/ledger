<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');

include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

require_once __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

error_reporting(E_ALL);

$helper = new Sample();

// Return to the caller script when runs by CLI
if ($helper->isCli()) {
    return;
}

$nilai	= $_POST['data'];

$inputFileName = $_SERVER['DOCUMENT_ROOT'].'/files/dataexcel/'.$nilai[0]['value'] ;
//$helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
$spreadsheet = IOFactory::load($inputFileName);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
$kln_id  = $sheetData[1]['B'] ;
$smbdata = $sheetData[2]['B'] ;
$tahun   = $sheetData[3]['B'] ;
$ak_1    = $sheetData[4]['B'] ;
$nk_1    = $sheetData[4]['C'] ;
$ak_2    = $sheetData[5]['B'] ;
$nk_2    = $sheetData[5]['C'] ;
$ak_3    = $sheetData[6]['B'] ;
$nk_3    = $sheetData[6]['C'] ;

$rows    =count($sheetData) ;
$query = "INSERT INTO tm_buku_bank (bak_akun,bk_bukti,bk_tanggal,bk_tahun,bk_nm_akun,bk_keterangan,bk_debet,bk_kredit,bk_smb_data,kln_id,bk_customer) VALUES" ;
for ($i=9; $i <= $rows; $i++) {
  $query .= "('".$ak_1."','".$sheetData[$i]['B']."','".$sheetData[$i]['A']."','$tahun','".$nk_1."','".$sheetData[$i]['C']."','0','".$sheetData[$i]['F']."','$smbdata','$kln_id','".$sheetData[$i]['G']."')," ;
  $query .= "('".$ak_2."','".$sheetData[$i]['B']."','".$sheetData[$i]['A']."','$tahun','".$nk_2."','".$sheetData[$i]['C']."','".$sheetData[$i]['D']."','0','".$smbdata." DPP','$kln_id','".$sheetData[$i]['G']."')," ;
  $query .= "('".$ak_3."','".$sheetData[$i]['B']."','".$sheetData[$i]['A']."','$tahun','".$nk_3."','".$sheetData[$i]['C']."','".$sheetData[$i]['E']."','0','".$smbdata." PPN','$kln_id','".$sheetData[$i]['G']."')" ;

  if(($i+1)<($rows + 1)){
    $query	.= ",";
  }
}
//echo $query ;



	$error		= "";
		try{
			$PLINK->beginTransaction();
			$PLINK->exec($query) ;
			$jumlah  = count($sheetData)-5 ;
			$pesan 	 = "Data has been saved successfully";
			$kelas	 = "success";
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$jumlah  = "0" ;
			$pesan	 = "Data gagal disimpan ".$e->getMessage();
			$kelas	 = "error";
			$error	 = $e->getMessage();
		}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query,"total" => $jumlah);
	echo json_encode($pesan);
?>
