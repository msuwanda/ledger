    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<?php
  /* Database setup information */
  include $_SERVER['DOCUMENT_ROOT']."/conf/setDB01.php";

  $que 	 = "SELECT tm_coa.kode_akun,tm_coa.nama_akun,tm_coa.coa_saldo_awal, tm_coa.coa_rumus FROM tm_coa WHERE tm_coa.kln_id = 100000  AND tm_coa.coa_tahun = 2016" ;
  $fetch = $PLINK->query($que);
  while ($row = $fetch->fetch_array()) {

    ?>
    <div class="page-wrapper">
        <div class="container-fluid">
        <span style="text-align:center"><h1><?= $row['kode_akun'] ?> - <?= $row['nama_akun'] ?></span>
        <table class="table table-hover table-bordered table-striped dataTable">
            <thead>
                <tr>
                    <th width="15%" >Tanggal</th>
                    <th>No. Bukti</th>
                    <th>Keterangan</th>
                    <th>Debet</th>
                    <th>Kredit</th>
                    <th>Saldo</th>
                    <th>Sumber Trans</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  $que2 	 = "SELECT * FROM tm_buku_bank WHERE kln_id = 100000  AND bk_tahun = 2016 AND bak_akun = '".$row['kode_akun']."' " ;
                  $fetch2  = $PLINK->query($que2);
                  while ($row2 = $fetch2->fetch_array()) {

                  ?>
                  <tr>
                      <td><?= $row2['bk_tanggal'] ?></td>
                      <td><?= $row2['bak_akun'] ?></td>
                      <td><?= $row2['bk_keterangan'] ?></td>
                      <td><?= $row2['bk_debet'] ?></td>
                      <td><?= $row2['bk_kredit'] ?></td>
                      <td></td>
                      <td><?= $row2['bk_smb_data'] ?></td>
                  </tr>

                  <?php } ?>
                </tbody>
                <tfoot>

                </tfoot>
         </table>
<?php } ?>
</div>
</div>
