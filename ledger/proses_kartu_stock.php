<?php
include $_SERVER['DOCUMENT_ROOT']."/conf/setDB01.php";
require_once __DIR__ . '/vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


$helper = new Sample();
if ($helper->isCli()) {
    $helper->log('This example should only be run from a Web Browser' . PHP_EOL);

    return;
}
// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document propertiesas
$spreadsheet->getProperties()->setCreator('Muhamamd Suwanda')
    ->setLastModifiedBy('spectra-app')
    ->setTitle('Excel buku besar')
    ->setSubject('Office 2007 XLSX Test Document')
    ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
    ->setKeywords('office 2007 openxml php')
    ->setCategory('Test result file');

// Add some data
    $que 	 = "SELECT item_kode,item_barang FROM tr_item WHERE kln_id = '".$_GET['kid']."'  AND item_tahun = '".$_GET['thn']."' GROUP BY item_kode" ;
    $fetch = $PLINK->query($que);
    $i = 1 ;
    $body = [
    'font' => [
        'bold' => false,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $tittle = [
    'font' => [
        'bold' => true,
    ],];
    $header = [
    'font' => [
        'bold' => true,
    ],
    'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];
    $foot = [
    'font' => [
        'bold' => true,
    ],
    'borders' => [
        'allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,],
    ], ];


   while ($row = $fetch->fetch_array()) {
      $k = $i + 1 ;
      $c = $i + 2 ;
      $h = $i + 5 ;
      $f = $i + 4 ;
    // tittle
    $spreadsheet->setActiveSheetIndex(0)
          ->setCellValue('A'.$i, $_GET['kn'])
          ->setCellValue('L'.$i, 'FILTER')
          ->setCellValue('A'.$k, $row['item_kode']." - ".$row['item_barang'])
          ->setCellValue('A'.$c, "KARTU STOCK - ".$_GET['thn']);
    $spreadsheet->getActiveSheet()->getStyle('A'.$i.':A'.$c)->applyFromArray($tittle);
    // header
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('C'.$f, 'MASUK')
        ->setCellValue('F'.$f, 'KELUAR')
        ->setCellValue('I'.$f, 'SALDO')
        ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$f.':K'.$f)->applyFromArray($header);
    $spreadsheet->getActiveSheet()->mergeCells('A'.$f.':B'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('C'.$f.':E'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('F'.$f.':H'.$f);
    $spreadsheet->getActiveSheet()->mergeCells('I'.$f.':K'.$f);
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$h, 'TANGGAL')
        ->setCellValue('B'.$h, 'KODE TRANS')
        ->setCellValue('C'.$h, 'Qty')
        ->setCellValue('D'.$h, 'Satuan')
        ->setCellValue('E'.$h, 'Total')
        ->setCellValue('F'.$h, 'Qty')
        ->setCellValue('G'.$h, 'Satuan')
        ->setCellValue('H'.$h, 'Total')
        ->setCellValue('I'.$h, 'Qty')
        ->setCellValue('J'.$h, 'Satuan')
        ->setCellValue('K'.$h, 'Total')
        ;
    $sd = $i + 6 ;
    $spreadsheet->getActiveSheet()->getStyle('A'.$h.':K'.$h)->applyFromArray($header);
    $spreadsheet->getActiveSheet()->getStyle('A'.$sd.':K'.$sd)->applyFromArray($header);
    // saldo awal
          $a = $i + 7 ;
          $que2 	 = "SELECT * FROM tr_item WHERE item_kode = '".$row['item_kode']."' AND kln_id = '".$_GET['kid']."'  AND item_tahun = '".$_GET['thn']."' ORDER BY item_tgl ASC,type ASC" ;
          $fetch2  = $PLINK->query($que2);

      // body
          while ($row2 = $fetch2->fetch_array()) {
            $g = $a - 1 ;
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('A'.$a, $row2['item_tgl'])
                ->setCellValue('B'.$a, $row2['kode_trans'])
                ->setCellValue('C'.$a, $row2['qm'])
                ->setCellValue('D'.$a, '=IF(C'.$a.'<>0,SUM(E'.$a.'/C'.$a.'),"")')
                ->setCellValue('E'.$a, $row2['tm'])
                ->setCellValue('F'.$a, $row2['qk'])
                ->setCellValue('G'.$a, '=IF(F'.$a.'<>0,+J'.$g.',"")')
                ->setCellValue('H'.$a, '=IF(F'.$a.'<>0,SUM(F'.$a.'*G'.$a.'),0)')
                ->setCellValue('I'.$a, '=SUM(I'.$g.'+C'.$a.'-F'.$a.')')
                ->setCellValue('J'.$a, '=IF(I'.$a.'<>0,SUM(K'.$a.'/I'.$a.'),0)')
                ->setCellValue('K'.$a, '=SUM(K'.$g.'+E'.$a.'-H'.$a.')')
                ;
            $spreadsheet->getActiveSheet()->getStyle('A'.$a.':K'.$a)->applyFromArray($body);
            $a++;
          }
          $sa = $a  ;
          $sb = $a - 1  ;
          // saldo akhir
          $spreadsheet->setActiveSheetIndex(0)
              ->setCellValue('A'.$sa, '')
              ->setCellValue('B'.$sa, '')
              ->setCellValue('C'.$sa, '')
              ->setCellValue('G'.$sa, '')
              ;

        $spreadsheet->getActiveSheet()->getStyle('A'.$sa.':K'.$sa)->applyFromArray($foot);
      $i = $a + 2;}

// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('Ledger');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="KARTU STOCK - '.$_GET['kn'].'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');


$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
