<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

<div class="row">
    <div class="col-12">
        <div class="card wizard-content card-outline-info ">
            <div class="card-header">
                <h4 class="m-b-0 text-white">form upload</h4>
            </div>
            <div class="card-block">
                <form action="bin/upload_format_saldo.php" method="post" role="form" enctype="multipart/form-data">
                      <div class="row" id="body-form" >
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label for="wfirstName2">Files 1</label>
                                  <input type="file" class="form-control" name="files[]">
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group">
                                 <button type="button" id="button-file" class="btn btn btn-warning"><i class="glyphicon glyphicon-floppy-disk"></i>Tambah From</button>
                                 <button type="submit" id="button-add" class="btn btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Proses Upload</button></div>
                          </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
