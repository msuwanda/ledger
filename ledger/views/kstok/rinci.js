dataBases    = JSON.parse(localStorage.databases);

$(document).ready(function() {
	//kunci    = JSON.parse(localStorage.karyawan);
	var dt = $('#tableuser').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
   		"ordering": false,
		"sAjaxSource": "/api/ledger/view_kartu_stok.php?id=" + getParam.id + "&t=" + dataBases.tahun + "&k=" + dataBases.kln_id + "&p1=" + dataBases.idpem + "&p2=" + dataBases.idhut + "&s=" + getParam.s ,
    	"sSearch": false,
		"searching": false ,
		"paging":   false,
		dom: 'Bfrtip',
		buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
		],
    	"columns":[
            {
						"class":          "details-control",
						"orderable":      false,
						"data":           "0",
						"defaultContent": ""
					 },
            { "data": "1","orderable": false },
            { "data": "2","orderable": false },
            { "data": "3","orderable": false },
            { "data": "4","orderable": false },
            { "data": "5","orderable": false  },
            { "data": "6","orderable": false },
            { "data": "7","orderable": false },
            { "data": "8","orderable": false  },
            { "data": "9","orderable": false },
            { "data": "10","orderable": false },
           ]
	} );
/*
	var detailRows = [];

	$('#datatable tbody').on( 'click', 'tr td.details-control', function () {
			var data = dt.row( $(this).parents('tr') ).data();
			window.location.href = "index.php?pages=401020&id="+ data[8] +"";
	} );
*/
	$('#datatable').attr('style', 'width:100%;');
} );
