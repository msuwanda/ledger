<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<style>
  table {
     font-size: 14px !important ;
     line-height: 1;
  }
  @media print {
    .left-sidebar {
      display: none;
    }
    .topbar {
      display: none;
    }
    .page-wrapper {
      padding-top: 0px;
    }
  }
</style>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <table class="table table-hover table-bordered dataTable" id="tableuser">
                  <thead>
                    <tr>
                      <th></th>
                      <th></th>
                      <th colspan="3" style="text-align: center; background:#f2f4f8 ;">Masuk</th>
                      <th colspan="3" style="text-align: center;">Keluar</th>
                      <th colspan="3" style="text-align: center; background:#f2f4f8 ;">Saldo</th>
                    </tr>
                    <tr>
                			<th>Tanggal</th>
                			<th>ITEM</th>
                      <th style="text-align: center; background:#f2f4f8 ;">Qty</th>
                      <th style="text-align: center; background:#f2f4f8 ;">Satuan</th>
                      <th style="text-align: center; background:#f2f4f8 ;">Jumlah</th>
                      <th>qty</th>
                      <th>Satuan</th>
                      <th>Jumlah</th>
                      <th>Qty</th>
                      <th>Satuan</th>
                      <th>Jumlah</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
