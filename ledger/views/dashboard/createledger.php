<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <div class="col-12">
        <div class="card card-outline-info">
       	 	<div class="card-header">
                <h4 class="m-b-0 text-white">form tambah</h4>
            </div>
            <div class="card-block">
              <form onsubmit="return false;" method="post" role="form">
		            <div class="row">
                  <div class="col-md-4">
  			            <div class="form-group">
  			              <label>Klien</label>
  			                 <input type='text' name='kln_id' class='form-control' id="klien_id">
  			            </div>
  			          </div>
                  <div class="col-md-8">
  			            <div class="form-group">
  			              <label>Klien</label>
  			                 <input type='text' name='kln_nama' class='form-control' id="klien_nama" readonly>
  			            </div>
  			          </div>
  			          <div class="col-md-12">
  			            <div class="form-group">
                      <label >Tahun</label>
                      <select name='masa_tahun' class='form-control'>
                        <option value="0">-</option>
                        <?php for ($i=2014; $i <= 2020; $i++) {
                        ?>
                          <option value="<?= $i ?>"><?= $i ?></option>
                        <?php
                        }
                        ?>
                      </select>
  			            </div>
  			          </div>
			        </div>
			      	<div class="row">
			          <div class="col-md-12">
			            <div class="form-group">
			              <div>
			                <button type="submit" id="button-simpan" class="btn btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
			              </div>
			            </div>
			          </div>
			        </div>
		        </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
