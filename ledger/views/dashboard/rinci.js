jQuery('#kln').html(getParam.n) ;
jQuery('#akun').html(getParam.akun + ' - ' + getParam.nakun) ;
jQuery('#tahun').html("BUKU BESAR - " + getParam.t) ;

$(document).ready(function() {
	//kunci    = JSON.parse(localStorage.karyawan);
	var dt = $('#datatable').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
    "ordering": false,
		"sAjaxSource": "/api/ledger/view_ledger.php?id=" + getParam.akun + "&t=" + getParam.t + "&k=" + getParam.id,
    "sSearch": false,
		"searching": false ,
		"paging":   false,
    "columns":[
            {
						"class":          "details-control",
						"orderable":      false,
						"data":           "0",
						"defaultContent": ""
					 },
            { "data": "1","orderable": false },
            { "data": "2","orderable": false },
            { "data": "3","orderable": false },
            { "data": "4","orderable": false },
            { "data": "7","orderable": false  },
            { "data": "5","orderable": false },
           ]
	} );
/*
	var detailRows = [];

	$('#datatable tbody').on( 'click', 'tr td.details-control', function () {
			var data = dt.row( $(this).parents('tr') ).data();
			window.location.href = "index.php?pages=401020&id="+ data[8] +"";
	} );
*/
	$('#datatable').attr('style', 'width:100%;');
} );

targetUrl = "/api/ledger/view_akun.php" ;

jQuery.post(targetUrl, {filter : [{name: "bk_tahun" ,value: getParam.t},{name: "kln_id" ,value: getParam.id},{name: "bak_akun" ,value: getParam.akun}]}, function(data){
  jQuery.each(data,function(i,value){

    // defining kontrol proses
    jQuery('#datatable tfoot').html('<tr><th colspan="3">Total</th><th>' + value.bak_debet + '</th><th>' + value.bak_kredit + '</th><th>' + value.bak_total + '</th><th></th></tr>');

  });
}, "json");
