dataBases    = JSON.parse(localStorage.databases);
jQuery('#page-titles').removeClass('hidden') ;

$(document).ready(function() {
	//kunci    = JSON.parse(localStorage.karyawan);
	var dt = $('#datatable').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
    "ordering": false,
		"sAjaxSource": "/api/ledger/supl.php?t=" + dataBases.tahun + "&k=" + dataBases.kln_id,
    "sSearch": false,
    "columns":[
            {
						"class":          "details-control",
						"orderable":      false,
						"data":           "0",
						"defaultContent": ""
					 },
            { "data": "1","orderable": false },
            { "data": "2","orderable": false },
           ]
	} );
	$('#datatable').attr('style', 'width:100%;');
} );
