<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

<div class="row">
    <div class="col-12">
        <div class="card wizard-content card-outline-info ">
            <div class="card-header">
                <h4 class="m-b-0 text-white">form Tambah</h4>
            </div>
            <div class="card-block">
                <form onsubmit="return false;" method="post" role="form">
                      <div class="row">
														<div class="col-md-12">
														   <div class="form-group">
														        <label>id</label>
														        <input type="text" class="form-control" name="kln_id">
														   </div>
														</div>
														<div class="col-md-12">
														   <div class="form-group">
														        <label>tahun</label>
														        <input type="text" class="form-control" name="bb_tahun">
														   </div>
														</div>
														<div class="col-md-12">
														   <div class="form-group">
														        <label>penjualan</label>
														        <input type="text" class="form-control" name="bb_penjualan">
														   </div>
														</div>
														<div class="col-md-12">
														   <div class="form-group">
														        <label>piutang</label>
														        <input type="text" class="form-control" name="bb_piutang">
														   </div>
														</div>
														<div class="col-md-12">
														   <div class="form-group">
														        <label>pembelian</label>
														        <input type="text" class="form-control" name="bb_pembelian">
														   </div>
														</div>
														<div class="col-md-12">
														   <div class="form-group">
														        <label>hutang</label>
														        <input type="text" class="form-control" name="bb_hutang">
														   </div>
														</div>
														<div class="col-md-12">
														   <div class="form-group">
														        <label>valas</label>
														        <input type="text" class="form-control" name="bb_valas">
														   </div>
														</div>

                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                 <button type="submit" id="button-add" class="btn btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button></div>
                          </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
