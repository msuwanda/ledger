dataBases    = JSON.parse(localStorage.databases);
jQuery('#kln').html(dataBases.kln_nama) ;
jQuery('#akun').html(getParam.akun + ' - ' + getParam.nakun) ;
jQuery('#tahun').html("BUKU BESAR - " + dataBases.tahun) ;
document.title = getParam.akun + ' - ' + getParam.nakun ;

$(document).ready(function() {
	//kunci    = JSON.parse(localStorage.karyawan);

	var dt = $('#datatable').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
    "ordering": false,
		"sAjaxSource": "/api/ledger/view_ledger_2.php?id=" + getParam.akun + "&t=" + dataBases.tahun + "&k=" + dataBases.kln_id + "&s=" + getParam.s,
    "sSearch": false,
		"searching": false ,
		"paging":   false,
		dom: 'Bfrtip',
		buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
    "columns":[
            {
						"class":          "details-control",
						"orderable":      false,
						"data":           "0",
						"defaultContent": ""
					 },
            { "data": "1","orderable": false },
            { "data": "2","orderable": false },
            { "data": "3","orderable": false },
            { "data": "4","orderable": false },
            { "data": "7","orderable": false },
            { "data": "5","orderable": false },
					],

	} );
/*
	var detailRows = [];

	$('#datatable tbody').on( 'click', 'tr td.details-control', function () {
			var data = dt.row( $(this).parents('tr') ).data();
			window.location.href = "index.php?pages=401020&id="+ data[8] +"";
	} );
*/
	$('#datatable').attr('style', 'width:100%;');
} );
