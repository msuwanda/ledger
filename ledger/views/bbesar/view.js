dataBases    = JSON.parse(localStorage.databases);
jQuery('#page-titles').removeClass('hidden') ;
jQuery('#sub_bread').html('Chart Of Account') ;
jQuery('#page-titles .col-4').append('<a href="proses_excel_2.php?kn='+ dataBases.kln_nama +'&kid='+ dataBases.kln_id +'&thn='+ dataBases.tahun +'" class="btn pull-right btn-success text-white"><i class="mdi mdi-network-download"></i> Download Buku Besar</a>') ;

targetUrl = "/api/ledger/view_akun.php" ;
debet = 0 ;
kredit = 0 ;
saldo = 0 ;
total = 0 ;
rows = '';
Number.prototype.formatMoney = function(c, d, t){
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
jQuery.post(targetUrl, {filter : [{name: "bk_tahun" ,value: dataBases.tahun},{name: "kln_id" ,value: dataBases.kln_id}]}, function(data){
  jQuery.each(data,function(i,value){

    // defining kontrol proses
    if (value.coa_rumus == 2) {
      vdebet  = value.kredit  ;
      vkredit = value.debet   ;

    }
    else {
      vdebet  = value.debet   ;
      vkredit = value.kredit  ;
    }
    vtotal = parseInt(value.coa_saldo_awal) + parseInt(vdebet) - parseInt(vkredit) ;
    rows += '<tr onclick="document.location.href=\'index.php?pages=114020&akun=' + value.bak_akun + '&nakun=' + value.nama_akun + '&s=' + value.coa_saldo_awal + '&r=' + value.coa_rumus + '\'" style=\"cursor: pointer\"><td>' + value.bak_akun + '</td><td>' + value.nama_akun + '</td><td>' + parseInt(value.coa_saldo_awal).formatMoney(2, '.', ',') + '</td><td>' + parseInt(vdebet).formatMoney(2, '.', ',') +  '</td><td>' + parseInt(vkredit).formatMoney(2, '.', ',') + '</td><td>' + parseInt(vtotal).formatMoney(2, '.', ',') + '</td></tr>' ;
    debet = (parseInt(debet) + parseInt(value.debet)) ;
    kredit = (parseInt(kredit) + parseInt(value.kredit)) ;
    saldo = (parseInt(saldo) + parseInt(value.coa_saldo_awal)) ;
    total = (parseInt(total) + parseInt(vtotal)) ;
  });
  // storing kontrol proses
  // rendering tabel
  rows += + '<tr>'
            + '<th >TOTAL</th>'
            + '<th ></th>'
            + '<th style="text-align:right" >'+ saldo.formatMoney(2, '.', ',') +'</th>'
            + '<th style="text-align:right" >'+ debet.formatMoney(2, '.', ',') +'</th>'
            + '<th style="text-align:right" >'+ kredit.formatMoney(2, '.', ',') +'</th>'
            + '<th style="text-align:right" >'+ total.formatMoney(2, '.', ',') +'</th>'
          + '</tr>' ;
  jQuery('#tableuser tbody').append(rows) ;

  $(document).ready(function() {
    $("#tableuser").DataTable({
      "paging":   false,
      dom: 'Bfrtip',
  		buttons: [
        { extend: 'copy', footer: true },
        { extend: 'csv', footer: true },
        { extend: 'excel', footer: true },
        { extend: 'pdf', footer: true },
        { extend: 'print', footer: true }],
    });
  } );
}, "json");
