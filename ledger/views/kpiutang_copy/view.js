jQuery('#page-titles').removeClass('hidden') ;
jQuery('#sub_bread').html('Rekap kartu Piutang') ;

dataBases    = JSON.parse(localStorage.databases);
jQuery('#page-titles .col-4').append('<a href="proses_kartu_piutang.php?kn='+ dataBases.kln_nama + '&ka='+ dataBases.id_piu +'&kid='+ dataBases.kln_id +'&thn='+ dataBases.tahun +'" class="btn pull-right btn-success text-white"><i class="mdi mdi-network-download"></i> Download Kartu Piutang</a>') ;

targetUrl = "/api/ledger/rekap_piutang.php" ;
saldo_awal = 0 ;
debet = 0 ;
kredit = 0 ;
Number.prototype.formatMoney = function(c, d, t){
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
jQuery.post(targetUrl, {filter : [{name: "kln_id" ,value: dataBases.kln_id},{name: "tahun" ,value: dataBases.tahun},{name: "tahun" ,value: dataBases.id_piu},{name: "tahun" ,value: dataBases.idpiu}]}, function(data){
  console.log(data);

  jQuery.each(data,function(i,value){

    // defining kontrol proses
    jQuery('#tableuser tbody').append('<tr onclick="document.location.href=\'index.php?pages=115030&akun=' + value.bk_customer + '&nakun=' + value.sup_nama + '&s=' + value.sup_saldo_awal + '\'" style=\"cursor: pointer\"><td>' + value.bk_customer + '</td><td>' + value.sup_nama + '</td><td style="text-align: right;">' + parseInt(value.sup_saldo_awal).formatMoney(2, '.', ',') + '</td><td style="text-align: right;">' + parseInt(value.debet).formatMoney(2, '.', ',') + '</td><td style="text-align: right;">' + parseInt(value.kredit).formatMoney(2, '.', ',') +  '</td><td style="text-align: right;">' + parseInt(value.total).formatMoney(2, '.', ',') +  '</td></tr>');
    saldo_awal = (parseInt(saldo_awal) + parseInt(value.sup_saldo_awal)) ;
    debet = (parseInt(debet) + parseInt(value.debet)) ;
    kredit = (parseInt(kredit) + parseInt(value.kredit)) ;
  });
  // storing kontrol proses
  // rendering tabel
  rows = + '<tr>'
            + '<th colspan="2" style="text-align:right">TOTAL</th>'
            + '<th style="text-align:right" >'+ saldo_awal.formatMoney(2, '.', ',') +'</th>'
            + '<th style="text-align:right" >'+ debet.formatMoney(2, '.', ',') +'</th>'
            + '<th style="text-align:right" >'+ kredit.formatMoney(2, '.', ',') +'</th>'
            + '<th style="text-align:right">'+ ( saldo_awal + kredit - debet).formatMoney(2, '.', ',') +'</th>'
          + '</tr>' ;
  jQuery('#tableuser tfoot').append(rows) ;
  $(document).ready(function() {
    $("#tableuser").DataTable({
      "paging":   false,
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel', 'pdf', 'print'
      ]
    });
  } );
}, "json");
