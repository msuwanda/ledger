<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');

include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

require_once __DIR__ . '/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Helper\Sample;

error_reporting(E_ALL);

$helper = new Sample();

// Return to the caller script when runs by CLI
if ($helper->isCli()) {
    return;
}

$nilai	= $_POST['data'];

$inputFileName = $_SERVER['DOCUMENT_ROOT'].'/files/dataexcel/'.$nilai[0]['value'] ;
//$helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
$spreadsheet = IOFactory::load($inputFileName);
$sheetData 	 = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
$kln_id   	 = $sheetData[1]['B'] ;
$tahun    	 = $sheetData[2]['B'] ;
$rows     	 = count($sheetData) ;
$query = "INSERT INTO tr_item (kode_trans,item_kode,item_tgl,item_barang,item_qty,item_total,kln_id,item_tahun,qk,tk,type) VALUES" ;
for ($i=5; $i <= $rows; $i++) {
  $query .= "('".$sheetData[$i]['B']."','".$sheetData[$i]['C']."','".$sheetData[$i]['A']."','".$sheetData[$i]['D']."','".$sheetData[$i]['E']."','".$sheetData[$i]['F']."','".$kln_id."','".$tahun."','".$sheetData[$i]['E']."','".$sheetData[$i]['F']."','2')" ;
  if($i<($rows)){
    $query	.= ",";
  }
}

	$error		= "";
		try{
			$PLINK->beginTransaction();
			$PLINK->exec($query) ;
			$jumlah  = count($sheetData)-6 ;
			$pesan 	 = "Data has been saved successfully And ".$jumlah." data has been created by the system";
			$kelas	 = "success";
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$jumlah  = "0" ;
			$pesan	 = "Data gagal disimpan ".$e->getMessage()." ".$query;
			$kelas	 = "error";
			$error	 = $e->getMessage();
		}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$query,"total" => $jumlah);
	echo json_encode($pesan);
?>
