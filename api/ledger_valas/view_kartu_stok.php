<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');
/**
 * Script:    DataTables server-side script for PHP 5.2+ and MySQL 4.1+
 * Notes:     Based on a script by Allan Jardine that used the old PHP mysql_* functions.
 *            Rewritten to use the newer object oriented mysqli extension.
 * Copyright: 2010 - Allan Jardine (original script)
 *            2012 - Kari Söderholm, aka Haprog (updates)
 * License:   GPL v2 or BSD (3-point)
 */


/**
 * Array of database columns which should be read and sent back to DataTables. Use a space where
 * you want to insert a non-database field (for example a counter or static image)
 */
$aColumns = array( 'item_tgl', 'item_barang', 'qm', 'tm', 'qk','kode_trans','ks_kurs');

// Indexed column (used for fast and accurate table cardinality)
$sIndexColumn = 'id';

// DB table to use
$sTable = 'v_item_valas';

include $_SERVER['DOCUMENT_ROOT']."/conf/setDBSS.php";

// Input method (use $_GET, $_POST or $_REQUEST)
$input =& $_GET;

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/**
 * Character set to use for the MySQL connection.
 * MySQL will return all strings in this charset to PHP (if the data is stored correctly in the database).
 */
$gaSql['charset']  = 'utf8';

/**
 * MySQL connection
 */
$db = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db'], $gaSql['port']);
if (mysqli_connect_error()) {
    die( 'Error connecting to MySQL server (' . mysqli_connect_errno() .') '. mysqli_connect_error() );
}

if (!$db->set_charset($gaSql['charset'])) {
    die( 'Error loading character set "'.$gaSql['charset'].'": '.$db->error );
}


/**
 * Paging
 */
$sLimit = "";
if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
    $sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
}


/**
 * Ordering
 */
$aOrderingRules = array();
/*
if ( isset( $input['iSortCol_0'] ) ) {
    $iSortingCols = intval( $input['iSortingCols'] );
    for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
        if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
            $aOrderingRules[] =
                "`".$aColumns[ intval( $input['iSortCol_'.$i] ) ]."` "
                .($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
        }
    }
}
*/
if (!empty($aOrderingRules)) {
    $sOrder = " ORDER BY item_tgl ASC,type ASC ";
} else {
    $sOrder = " ORDER BY item_tgl ASC,type ASC ";
}


/**
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$iColumnCount = count($aColumns);

if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
    $aFilteringRules = array();
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
        if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
            $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string( $input['sSearch'] )."%'";
        }
    }
    if (!empty($aFilteringRules)) {
        $aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
    }
}

// Individual column filtering
for ( $i=3 ; $i<$iColumnCount ; $i++ ) {
    if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
        $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string($input['sSearch_'.$i])."%'";
    }
}
// filtering manual
/*
if ( isset($input['bSearchable_0']) && $input['bSearchable_0'] == 'true' && $input['sSearch_0'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[0]."` LIKE '%".$db->real_escape_string($input['sSearch_0'])."%'";
    }
if ( isset($input['bSearchable_1']) && $input['bSearchable_1'] == 'true' && $input['sSearch_1'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[1]."` = '".$db->real_escape_string($input['sSearch_1'])."'";
    }
if ( isset($input['bSearchable_2']) && $input['bSearchable_2'] == 'true' && $input['sSearch_2'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[2]."` = '".$db->real_escape_string($input['sSearch_2'])."'";
    }
*/
// end filtering manual
//$sWhere = "WHERE kln_id = '".$_GET['k']."' AND bk_tahun = '".$_GET['t']."'" ;
$sWhere = "WHERE kln_id = '".$_GET['k']."' AND item_tahun = '".$_GET['t']."' AND item_kode = '".$_GET['id']."'" ;
if (!empty($aFilteringRules)) {
    $sWhere .= " AND ".implode(" AND ", $aFilteringRules);
} else {
    $sWhere .= "";
}


/**
 * SQL queries
 * Get data to display
 */
$aQueryColumns = array();
foreach ($aColumns as $col) {
    if ($col != ' ') {
        $aQueryColumns[] = $col;
    }
}

$sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", $aQueryColumns)."`
    FROM `".$sTable."`".$sWhere.$sOrder.$sLimit;

$rResult = $db->query( $sQuery ) or die($db->error);

// Data set length after filtering
$sQuery = "SELECT FOUND_ROWS()";
$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

// Total data set length
$sQuery = "SELECT COUNT(`".$sIndexColumn."`) FROM `".$sTable."`".$sWhere;
$rResultTotal = $db->query( $sQuery ) or die($db->error);
list($iTotal) = $rResultTotal->fetch_row();


/**
 * Output
 */
$output = array(
    "sEcho"                => intval($input['sEcho']),
    "iTotalRecords"        => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData"               => array(),
    "aaTotal"               => array(),
);
//$total = (integer)$_GET['s'];
//$output['aaData'][] = ['-','-','-','-','-','Saldo Awal','-',number_format($total,2)] ;
$saldo_qty = 0 ;
$saldo_jumlah = 0 ;
$saldo_satuan = 0 ;
while ( $aRow = $rResult->fetch_assoc() ) {
    $row = array();
    $qty_masuk = (int)$aRow[$aColumns[2]] ;
    $harga_masuk = (int)$aRow[$aColumns[3]] * (int)$aRow[$aColumns[6]] ;
    $qty_keluar = (int)$aRow[$aColumns[4]] ;

    $harga_masuk_v = $harga_masuk * (int)$aRow[$aColumns[6]] ;
if ($qty_masuk == 0 ){ $satuan_m = ""; } else { $satuan_m = $harga_masuk/$qty_masuk; }
    $satuan_k = $saldo_satuan ;
if ($qty_keluar == 0 ) { $satuan_k = "" ; } else { $satuan_k = $saldo_satuan ;}
    $harga_keluar = $satuan_k * $qty_keluar ;
    $saldo_qty = round($saldo_qty + $qty_masuk - $qty_keluar) ;
    $saldo_jumlah = $saldo_jumlah + $harga_masuk - $harga_keluar ;
if ($saldo_qty == 0 ) { $saldo_satuan = 0 ; } else { $saldo_satuan = $saldo_jumlah/$saldo_qty ; }

    $row[] = $aRow[$aColumns[0]]; // tgl
    $row[] = $aRow[$aColumns[1]]; // nama
    $row[] = $aRow[$aColumns[2]]; // qm
    $row[] = number_format($satuan_m,2)  ; // satuan
    $row[] = $harga_masuk; // tm
    $row[] = $aRow[$aColumns[4]]; // qk
    $row[] = number_format($satuan_k,2) ; // satuan
    $row[] = number_format($harga_keluar,2) ; //value_tk
    $row[] = $saldo_qty; // qty saldo
    $row[] = number_format($saldo_satuan,2) ; // satuan saldo
    $row[] = number_format($saldo_jumlah,2) ; // saldo
    // rupiah
    $row[] = number_format($aRow[$aColumns[6]],2); // kurs


    $output['aaData'][] = $row;
}

echo json_encode( $output );
//$aColumns = array( 'item_tgl', 'item_barang', 'qm', 'tm', 'qk','kode_trans');
