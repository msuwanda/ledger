<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');
/**
 * Script:    DataTables server-side script for PHP 5.2+ and MySQL 4.1+
 * Notes:     Based on a script by Allan Jardine that used the old PHP mysql_* functions.
 *            Rewritten to use the newer object oriented mysqli extension.
 * Copyright: 2010 - Allan Jardine (original script)
 *            2012 - Kari Söderholm, aka Haprog (updates)
 * License:   GPL v2 or BSD (3-point)
 */


/**
 * Array of database columns which should be read and sent back to DataTables. Use a space where
 * you want to insert a non-database field (for example a counter or static image)
 */
$aColumns = array( 'bk_tanggal','bak_akun','bk_nm_akun', 'bk_bukti', 'bk_keterangan','bk_debet', 'bk_kredit', 'bk_smb_data','coa_lawan', 'bk_id', );

// Indexed column (used for fast and accurate table cardinality)
$sIndexColumn = 'bk_id';

// DB table to use
$sTable = 'tm_buku_bank_valas';

include $_SERVER['DOCUMENT_ROOT']."/conf/setDBSS.php";

// Input method (use $_GET, $_POST or $_REQUEST)
$input =& $_GET;

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/**
 * Character set to use for the MySQL connection.
 * MySQL will return all strings in this charset to PHP (if the data is stored correctly in the database).
 */
$gaSql['charset']  = 'utf8';

/**
 * MySQL connection
 */
$db = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db'], $gaSql['port']);
if (mysqli_connect_error()) {
    die( 'Error connecting to MySQL server (' . mysqli_connect_errno() .') '. mysqli_connect_error() );
}

if (!$db->set_charset($gaSql['charset'])) {
    die( 'Error loading character set "'.$gaSql['charset'].'": '.$db->error );
}


/**
 * Paging
 */
$sLimit = "";
if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
    $sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
}


/**
 * Ordering
 */
$aOrderingRules = array();
/*
if ( isset( $input['iSortCol_0'] ) ) {
    $iSortingCols = intval( $input['iSortingCols'] );
    for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
        if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
            $aOrderingRules[] =
                "`".$aColumns[ intval( $input['iSortCol_'.$i] ) ]."` "
                .($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
        }
    }
}
*/
if (!empty($aOrderingRules)) {
    $sOrder = " ORDER BY bk_id ASC ";
} else {
    $sOrder = " ORDER BY bk_id ASC";
}


/**
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$iColumnCount = count($aColumns);

if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
    $aFilteringRules = array();
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
        if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
            $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string( $input['sSearch'] )."%'";
        }
    }
    if (!empty($aFilteringRules)) {
        $aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
    }
}

// Individual column filtering
for ( $i=3 ; $i<$iColumnCount ; $i++ ) {
    if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
        $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string($input['sSearch_'.$i])."%'";
    }
}
// filtering manual
/*
if ( isset($input['bSearchable_0']) && $input['bSearchable_0'] == 'true' && $input['sSearch_0'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[0]."` LIKE '%".$db->real_escape_string($input['sSearch_0'])."%'";
    }
if ( isset($input['bSearchable_1']) && $input['bSearchable_1'] == 'true' && $input['sSearch_1'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[1]."` = '".$db->real_escape_string($input['sSearch_1'])."'";
    }
if ( isset($input['bSearchable_2']) && $input['bSearchable_2'] == 'true' && $input['sSearch_2'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[2]."` = '".$db->real_escape_string($input['sSearch_2'])."'";
    }
*/
// end filtering manual
$sWhere = " WHERE kln_id = '".$_GET['k']."' AND bk_tahun = '".$_GET['t']."'" ;
if (!empty($aFilteringRules)) {
    $sWhere .= " AND ".implode(" AND ", $aFilteringRules);
} else {
    $sWhere .= "";
}


/**
 * SQL queries
 * Get data to display
 */
$aQueryColumns = array();
foreach ($aColumns as $col) {
    if ($col != ' ') {
        $aQueryColumns[] = $col;
    }
}

$sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", $aQueryColumns)."`
    FROM `".$sTable."`".$sWhere.$sOrder.$sLimit;

$rResult = $db->query( $sQuery ) or die($db->error);

// Data set length after filtering
$sQuery = "SELECT FOUND_ROWS()";
$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

// Total data set length
$sQuery = "SELECT COUNT(`".$sIndexColumn."`) FROM `".$sTable."`".$sWhere;
$rResultTotal = $db->query( $sQuery ) or die($db->error);
list($iTotal) = $rResultTotal->fetch_row();


/**
 * Output
 */
$output = array(
    "sEcho"                => intval($input['sEcho']),
    "iTotalRecords"        => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData"               => array(),
    "aaTotal"               => array(),
);
while ( $aRow = $rResult->fetch_assoc() ) {
    $row = array();
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
        if ( $aColumns[$i] == 'bk_kredit' ) {
            // Special output formatting for 'version' column
            $row[] = number_format($aRow[ $aColumns[$i] ], 2) ;
        } elseif ( $aColumns[$i] == 'bk_debet' ) {
            // Special output formatting for 'version' column
            $row[] = number_format($aRow[ $aColumns[$i] ], 2) ;
        } elseif ( $aColumns[$i] != ' ' ) {
            // General output
            $row[] = $aRow[ $aColumns[$i] ];
        }
    }
    //$total = round($total + $aRow[ $aColumns[3]] - $aRow[ $aColumns[4]], 2);
    //$row[] = number_format($total, 2) ;
    $output['aaData'][] = $row;
}

echo json_encode( $output );
