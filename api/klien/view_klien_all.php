<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Origin: *');

		/** getParam
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
$aColumns = array( 'kln_id', 'kln_nama','kln_kontak','kln_telepon','gu_perusahaan','team_nama','team_da',);

// Indexed column (used for fast and accurate table cardinality)
$sIndexColumn = 'kln_id';

// DB table to use
$sTable = 'v_klien';

// Database connection information
include $_SERVER['DOCUMENT_ROOT']."/conf/setDBSS.php";
// Input method (use $_GET, $_POST or $_REQUEST)
$input =& $_GET;

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/**
 * Character set to use for the MySQL connection.
 * MySQL will return all strings in this charset to PHP (if the data is stored correctly in the database).
 */
$gaSql['charset']  = 'utf8';

/**
 * MySQL connection
 */
$db = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db'], $gaSql['port']);
if (mysqli_connect_error()) {
    die( 'Error connecting to MySQL server (' . mysqli_connect_errno() .') '. mysqli_connect_error() );
}

if (!$db->set_charset($gaSql['charset'])) {
    die( 'Error loading character set "'.$gaSql['charset'].'": '.$db->error );
}


/**
 * Paging
 */
$sLimit = "";
if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
    $sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
}


/**
 * Ordering
 */
$aOrderingRules = array();
if ( isset( $input['iSortCol_0'] ) ) {
    $iSortingCols = intval( $input['iSortingCols'] );
    for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
        if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
            $aOrderingRules[] =
                "`".$aColumns[ intval( $input['iSortCol_'.$i] ) ]."` "
                .($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
        }
    }
}

if (!empty($aOrderingRules)) {
    $sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
} else {
    $sOrder = "";
}


/**
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$iColumnCount = count($aColumns);

if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
    $aFilteringRules = array();
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
        if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
            $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string( $input['sSearch'] )."%'";
        }
    }
    if (!empty($aFilteringRules)) {
        $aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
    }
}

// Individual column filtering
for ( $i=3 ; $i<$iColumnCount ; $i++ ) {
    if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
        $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string($input['sSearch_'.$i])."%'";
    }
}

// filtering manual
if ( isset($input['bSearchable_0']) && $input['bSearchable_0'] == 'true' && $input['sSearch_0'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[0]."` LIKE '%".$db->real_escape_string($input['sSearch_0'])."%'";
    }
if ( isset($input['bSearchable_1']) && $input['bSearchable_1'] == 'true' && $input['sSearch_1'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[1]."` = '".$db->real_escape_string($input['sSearch_1'])."'";
    }
if ( isset($input['bSearchable_2']) && $input['bSearchable_2'] == 'true' && $input['sSearch_2'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[2]."` = '".$db->real_escape_string($input['sSearch_2'])."'";
    }
// end filtering manual

if (!empty($aFilteringRules)) {
    $sWhere = " WHERE kln_status=1 AND ".implode(" AND ", $aFilteringRules);
} else {
    $sWhere = " WHERE kln_status=1 ";
}


/**
 * SQL queries
 * Get data to display
 */
$aQueryColumns = array();
foreach ($aColumns as $col) {
    if ($col != ' ') {
        $aQueryColumns[] = $col;
    }
}

$sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", $aQueryColumns)."`
    FROM `".$sTable."`".$sWhere.$sOrder.$sLimit;

$rResult = $db->query( $sQuery ) or die($db->error);

// Data set length after filtering
$sQuery = "SELECT FOUND_ROWS()";
$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

// Total data set length
$sQuery = "SELECT COUNT(`".$sIndexColumn."`) FROM `".$sTable."`";
$rResultTotal = $db->query( $sQuery ) or die($db->error);
list($iTotal) = $rResultTotal->fetch_row();


/**
 * Output
 */
$output = array(

    "iTotalRecords"        => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData"               => array(),
);
//$a = 1 ;
while ( $aRow = $rResult->fetch_assoc() ) {
    $row = array();
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
        if ( $aColumns[$i] == 'kln_nama' ) {
            // Special output formatting for 'version' column
            $dat = explode(".", $aRow[ $aColumns[$i]] ) ;
						$count = COUNT($dat) ;
						if ($count == '1') {
							$row[] = $dat[0];
						}
						else {
							$row[] = $dat[1].", ".$dat[0].".";
						}
			//	} elseif ( $aColumns[$i] == 'kln_id' ) {
            // General output
      //      $row[] = $a ;
        } elseif ( $aColumns[$i] == 'team_nama' ) {
            // General output
						if ($aRow[$aColumns[$i]] == '0') {
							$row[] = $aRow['team_da'] ;
						}else {
							$row[] = $aRow[$aColumns[$i]] ;
						}

        } elseif ( $aColumns[$i] != ' ' ) {
            // General output
            $row[] = $aRow[ $aColumns[$i] ];
        }
    }
    $output['aaData'][] = $row;
		//$a++ ;
}

echo json_encode( $output );

?>
