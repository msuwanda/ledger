<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/** getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['data'];

	for($i=0;$i<count($nilai);$i++){
		define("__".$nilai[$i]['name'],$nilai[$i]['value']) ;
	}
	$que	= "INSERT INTO tm_klien(kln_id,kln_nama,kln_alamat,kln_telepon,kln_email,kln_sumber_data,
									kln_kontak,lat,lng,gu_id,kln_npwp,kln_tgl_sertifikat,kln_tgl_pkp,
									kln_tgl_terdaftar,kln_tgl_masuk_spc,kpp_kode,kln_status)
					VALUES('".__kln_id."','".__kln_nama."','".__kln_alamat."','".__kln_telepon."','".__kln_email."',
							'".__kln_sumber_data."','".__kln_kontak."','".__lat."','".__lng."','".__gu_id."',
							'".__kln_npwp."','".__kln_tgl_sertifikat."','".__kln_tgl_pkp."','".__kln_tgl_terdaftar."','".__kln_tgl_masuk_spc."','".__kpp_kode."','1')";
	/* getParam **/

	$error		= "";
	if(strlen($nilai[0]['value'])>0){
		try{
			$PLINK->beginTransaction();
			if($PLINK->exec($que)>0){
				$title  = "Good Job!" ;
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "success";
				$url    = "301000" ;
				mkdir($_SERVER['DOCUMENT_ROOT'].'/files/'.__kln_id) ;
			}
			else{
				$title  = "Sorry !" ;
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "warning";
				$url    = "301010" ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$title  = "Sorry !" ;
			$pesan	= "Data gagal disimpan";
			$kelas	= "error";
			$url    = "301010" ;
			$error	= $e->getMessage();
		}
	}
	else{
		$title  = "Sorry !" ;
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "error";
		$url    = "301010" ;
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que, "url" => $url,"title" => $title);
	echo json_encode($pesan);
?>
