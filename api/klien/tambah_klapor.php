<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/** getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['data'];

	$que	= "INSERT INTO tr_klapor(kln_id,lp_id) VALUES";
	for($i=1;$i<count($nilai);$i++){
	$filter[] = "('".$_POST['data'][0]['value']."','".$_POST['data'][$i]['value']."')";
	}
	$que .= implode(',',$filter) ;
	/* getParam **/

	$error		= "";
	if(strlen($nilai[0]['value'])>0){
		try{
			$PLINK->beginTransaction();
			$PLINK->exec("DELETE FROM tr_klapor WHERE kln_id='".$_POST['data'][0]['value']."';");
			if($PLINK->exec($que)>0){
				$title  = "Good Job!" ;
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "success";
				$url    = "301000" ;
			}
			else{
				$title  = "Sorry !" ;
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "warning";
				$url    = "301010" ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$title  = "Sorry !" ;
			$pesan	= "Data gagal disimpan";
			$kelas	= "error";
			$url    = "301010" ;
			$error	= $e->getMessage();
		}
	}
	else{
		$title  = "Sorry !" ;
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "error";
		$url    = "301010" ;
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que, "url" => $url,"title" => $title);
	echo json_encode($pesan);
?>
