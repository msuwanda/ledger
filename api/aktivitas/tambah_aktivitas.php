<?php

	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');
	session_start();
	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/** getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['data'];
  $ini   = 0 ;
	$data  = "(" ;
	$bagi  =  count($nilai) / 5 ;
  $count = 5 ;
	for($i=0;$i<count($nilai);$i++){
		//define("__".$nilai[$i]['name'],$nilai[$i]['value']) ;
		$data .= "'".$nilai[$i]['value']."'" ;
		if(($i+1)<count($nilai) and ($i+1) != $count){
			$data .=	",";
		}
	 if(($i+1) == $count AND (($i+1) / 5) != $bagi ){
			$data .=	",'".$_SESSION['User_c']."',NOW(),'1'),(";
			$count = $count + 5 ;
		}
	}

  $data .= ",'".$_SESSION['User_c']."',NOW(),'1')" ;

	$error		= "";
	if(strlen($nilai[0]['value'])>3){
		try{
			$PLINK->beginTransaction();
			$que	= "INSERT INTO tm_aktivitas(ak_tanggal,klien_id,ak_jam_s,ak_jam_f,ak_text,usr_id,ak_tgl_upload,ak_status) VALUES ".$data;
			if($PLINK->exec($que)>0){
				$title  = "Good Job!" ;
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "success";
				$url    = "603000" ;
			}
			else{
				$title  = "Sorry !" ;
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "warning";
				$url    = "601010" ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$title  = "Sorry !" ;
			$pesan	= "Data gagal disimpan";
			$kelas	= "error";
			$url    = "601010" ;
			$error	= $e->getMessage();
		}
	}
	else{
		$title  = "Sorry !" ;
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "error";
		$url    = "601010" ;
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que, "url" => $url,"title" => $title);
	echo json_encode($pesan);

?>
