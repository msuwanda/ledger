<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP| MySQL | React.js | Axios Example</title>
    <script src= "https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script src= "https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
    <!-- Load Babel Compiler -->
    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>
<body>
<div id="root">
</div>
</body>
</html>
<script type="text/babel">

class Show extends React.Component {
  state = {
    count:0,
    datas: []
  }
  handleNext = () => {
    this.setState({ count: this.state.count + 10})
  }
  handlePrev = () => {
    if(this.state.count !== 0 ) return this.setState({ count: this.state.count - 10})
  }
  componentDidMount = () => {
    axios({
        method: 'post',
        url: '/api/tracking/view_antar_react.php',
        data: JSON.stringify({filter: [{name: "suwanda",value: this.state.count}]}),
        config: { headers: {
//              'Content-Type': 'application/json',
//              'Content-Type': 'multipart/form-data',
            'Content-Type': 'application/x-www-form-urlencoded'
            }}
    })
    .then(res => {
        const datas = res.data;
        this.setState({ datas });
        console.log(res);
      })
  }
  render() {
    //console.log(this.state);
    return (
    <div>
      <h1>{this.state.count}</h1>
      <button onClick={this.handlePrev}>Prev</button>
      <button onClick={this.handleNext}>Next</button>
    </div> ) ;
  }
}
ReactDOM.render(
  <Show />,
  document.getElementById('root')
);

</script>
