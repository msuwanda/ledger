<?php
	session_start();
	error_reporting(0);
	date_default_timezone_set('Asia/Jakarta');
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/* getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai		= $_POST['data'];
	for($i=0;$i<count($nilai);$i++){
		define("__".$nilai[$i]['name'],$nilai[$i]['value']) ;
	}

	try{


			$e="[";
			for($i=3;$i<count($nilai)-2;$i++){
				$ex   =  explode(":",$nilai[$i]['value']);

				if($ex[0] == 9)
				{
					$a .='{"jdok_id":"'.$ex[0].'","jdok_nama":"'.$ex[1].' '.__fs_lain.'"},';
				}
				else {
					$a .='{"jdok_id":"'.$ex[0].'","jdok_nama":"'.$ex[1].'"},';
				}
			}
			$e .=substr($a,0,-1);
			$e .="]";
			$jdok=$PLINK->quote($e);

			$PLINK->beginTransaction();
			$que	= "INSERT INTO tm_filestream(fs_tgl,fs_tglr,kln_id,usr_id,fs_mess,fs_tipe,fs_jdokdesc,status_id,fs_desc)";
			$que	.= "VALUES('".__fs_tgl."',NOW(),'".__kln_id."','".$_SESSION['User_c']."',
						'00000000000','".__fs_tipe."','".substr($jdok,1,-1)."','0','".__fs_desc."')";

			if($PLINK->exec($que)>0){
				$title  = "Good Job!" ;
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "success";
				$url    = "502100" ;
			}
			else{
				$title  = "Sorry !" ;
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "warning";
				$url    = "502010" ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$title  = "Sorry !" ;
			$pesan	= "Data gagal disimpan";
			$kelas	= "error";
			$url    = "502010" ;
			$error	= $e->getMessage();
		}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que, "url" => $url,"title" => $title);
	echo json_encode($pesan);
?>
