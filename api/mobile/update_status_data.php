<?php
session_start();
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}
//database
include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";
$postdata = file_get_contents("php://input");
if (isset($postdata)) {
		$request = json_decode($postdata);
		$fs_id=$request->fs_id;
    $usr_id=$request->usr_id;
		$status_id = $request->status_id;
    $fs_sign= $request->fs_sign;
    $img=$request->img;
		$fs_nmsign=$request->fs_nmsign;
		$img_lapor= $request->fs_img;

		$query="UPDATE tm_filestream SET fs_img='".$fs_id."-KPP.jpg', fs_nmsign='".$fs_nmsign."', status_id='".$status_id."', usr_upd='".$usr_id."', fs_sign='".$fs_sign."', fs_tglr=NOW()  WHERE fs_id='".$fs_id."'";
	//query
		try{
			$PLINK->beginTransaction();
			if($PLINK->exec($query)>0){
				$pesan 	= "berhasil";
        if($img!=""){
          file_put_contents("../../files/data_sign/".$fs_sign, base64_decode(explode(',',$img)[1]));
				}
				if($img_lapor!=""){
          file_put_contents("../../files/imglapor/".$fs_id."-KPP.jpg", base64_decode(explode(',',$img_lapor)[1]));
        }
			}
			else{
				$pesan 	= "error0";
        $error  = $query ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
      $PLINK->rollBack();
			$pesan	= "error1";
			$error	= $e->getMessage();
		}
	}
	else {
		$pesan="error2";
	}
	#$log->logDB($que);
	#$log->logMess($pesan);
  $row  = array("pesan"=>$pesan);
  echo json_encode($row);
?>
