<?php

	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

		include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";
		/* getParam **/
			$filter	= "WHERE fs_mess='".$_GET['usr']."' ";

		/* database **/
		try {
			$que 	= "	SELECT 
				coalesce(sum( IF ( fs_tipe = 'antar' AND status_id = 1, 1, 0 ) ),0) AS antar_pend,
				coalesce(sum( IF ( fs_tipe = 'antar' AND status_id = 12, 1, 0 ) ),0) AS antar_prog,
				coalesce(sum( IF ( fs_tipe = 'ambil' AND status_id = 1, 1, 0 ) ),0) AS ambil_pend,
				coalesce(sum( IF ( fs_tipe = 'ambil' AND status_id = 12, 1, 0 ) ),0) AS ambil_prog,
				coalesce(sum( IF ( fs_tipe = 'lapor' AND status_id = 1, 1, 0 ) ),0) AS lapor_pend,
				coalesce(sum( IF ( fs_tipe = 'lapor' AND status_id = 12, 1, 0 ) ),0) AS lapor_prog 
						FROM v_filestream ".$filter." ORDER BY fs_tgl DESC" ;
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
			$PLINK 	= null;
		}
		catch (PDOException $e){
			$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
		}

	echo json_encode($row);
    flush();
?>
