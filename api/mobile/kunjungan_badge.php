<?php

	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";
	/* getParam **/
	$filter	= "WHERE k_mess='".$_GET['usr']."'";
	/* database **/
	try {
		$que 	= "SELECT	COALESCE ( sum( IF ( status_id = 1, 1, 0 ) ), 0 ) AS kunj_pend,
							COALESCE ( sum( IF ( status_id = 12, 1, 0 ) ), 0 ) AS kunj_prog 
							FROM v_kunjungan ".$filter." ORDER BY k_tgl DESC" ;
		$sth 	= $PLINK->prepare($que);
		$sth->execute();
		$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
		$PLINK 	= null;
	}
	catch (PDOException $e){
		$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
	}

	echo json_encode($row);
    flush();
?>
