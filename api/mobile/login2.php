<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
session_start();
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

//database
include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

$postdata = file_get_contents("php://input");
if (isset($postdata)) {
		$request = json_decode($postdata);
		$usr_id = $request->usr_id;
		$usr_pass= $request->usr_pass;
	//query
	try {
			$que 		= "SELECT * FROM v_login WHERE usr_id='".$usr_id."' AND usr_pass='".$usr_pass."'";
			$sth 		= $PLINK->prepare($que);
			$sth->execute();
			if($row	= $sth->fetch(PDO::FETCH_ASSOC)){
				$pesan="berhasil";
        $_SESSION['user_i']=$row['usr_id'];
				$nik= $row['nik'];
				$image= $row['usr_img'];
			}
			else{
				$pesan="error";
				$nik="";
				session_destroy();
			}
	} catch (Exception $e) {
			$pesan="error";
			$nik="";
			session_destroy();
	}
}
else {
	$pesan="error";
	$nik="";
	session_destroy();
}
$row = array("pesan"=>$pesan,"nik"=>$nik,"image"=>$image,"isi"=>$row);
echo json_encode($row);
 ?>
