<?php

		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
		
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		
			exit(0);
		}
		include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";
		
		/* getParam **/
		if(isset($_POST['filter'])){
			$filter	= array();
			$nilai	= $_POST['filter'];
			for($i=0;$i<count($nilai);$i++){
				$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			}
			if($i>0){
				$filter	= "WHERE ".implode(' AND ',$filter);
			}
		}
		else{
			$filter	= "";
		}
		/* database **/
		try {
			$que 	= "SELECT * FROM v_barang ".$filter." order by brg_id desc";
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
			$PLINK 	= null;
		}
		catch (PDOException $e){
			$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
		}

	echo json_encode($row);
    flush();
?>
