<?php
    /* Database setup information */
    include $_SERVER['DOCUMENT_ROOT']."/conf/setDB01.php";

    $return_arr = array();
    $param = $_GET["term"];

    $fetch = $PLINK->query("SELECT * FROM tm_karyawan WHERE nik REGEXP '^$param' OR k_nama REGEXP '$param' ");

    /* Retrieve and store in array the results of the query.*/
    while ($row = $fetch->fetch_array()) {

        $row_array['itemCode'] 		    = $row['nik'];
        $row_array['itemDesc'] 		    = $row['k_nama'];

        array_push( $return_arr, $row_array );
    }


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
?>
