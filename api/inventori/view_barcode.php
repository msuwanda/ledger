<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Float four columns side by side */
.column {
  float: left;
  width: 170px;
  padding: 10px 10px;
  
}

/* Remove extra left and right margins, due to padding */
.row {margin: 0 -5px;}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive columns */
@media screen and (max-width: 800px) {
  .column {
    width: 100%;
    display: block;
    margin-bottom: 2px;
  }
}

/* Style the counter cards */
.card {
    border-style: solid;
    border-width: 1px;
    padding: 1px;
    text-align: center;
}
font {
  font-size: 8px;
}
.pad {
  line-height: 15px;
}

</style>

</head>
<body>
<center>
<h2>INVENTORY BARANG</h2>
</center>
<hr>
<div class="row">
<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include $_SERVER['DOCUMENT_ROOT']."/assets/plugins/phpqrcode-master/qrlib.php";
include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

$que 	= "SELECT * FROM v_barang  order by brg_id desc";
$sth 	= $PLINK->prepare($que);
$sth->execute();

while($row	= $sth->fetch()){
	QRcode::png($row['brg_id'],"../../files/BRG_QRCODE/".$row['brg_id'].".png","H",5);
?>

  <div class="column">
  <font class="pad"><?=$row['k_nama']?></font>
    <div class="card">
    <table>
      <tr>
          <td><img src="../../files/BRG_QRCODE/<?=$row['brg_id']?>.png" width="50" height="53"></td>
          <td align="center">
              <img alt='testing' src='lib-barcode.php?codetype=Code128&size=40&text=<?=$row['brg_id']?>' height="27"/> <br> 
              <font><?=$row['brg_id']?> : <?=$row['jenis_nama']?></font>
          </td>
      </tr>
    </table>
    
    </div>
    
    
  </div>

<?php } ?>
</div>

</body>
</html>


