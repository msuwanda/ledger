<?php
	
	date_default_timezone_set('Asia/Jakarta');
	if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    session_start();
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
    
        exit(0);
    }

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/* getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
    */
    $input = $_POST ;
	$in = json_decode(file_get_contents("php://input"));
	$datas = '' ;
	$ard = array();
	for ($i=0; $i < count($in->data) ; $i++) {
		$ard[] = $in->data[$i]->name." = '".$in->data[$i]->value."'" ;
	}
	$datas = implode(",",$ard) ;
    try{
		$PLINK->beginTransaction();
		if($PLINK->exec("INSERT INTO tm_barang SET ".$datas)>0){
			$pesan 	= "berhasil";
		}
		else{
			$pesan 	= "Data tidak bisa disimpan";
		}
		$PLINK->commit();
	}
	catch(Exception $e){
		$PLINK->rollBack();
		$pesan	= $e->getMessage();
	}


	$pesan  = array("pesan"=>$pesan);
	echo json_encode($pesan);
?>
