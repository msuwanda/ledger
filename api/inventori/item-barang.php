<?php
    /* Database setup information */
    include $_SERVER['DOCUMENT_ROOT']."/conf/setDB01.php";

    $return_arr = array();
    $param = $_GET["term"];

    $fetch = $PLINK->query("SELECT * FROM v_barang WHERE brg_kode REGEXP '^$param' OR jenis_nama REGEXP '$param' OR k_nama REGEXP '$param' ");

    /* Retrieve and store in array the results of the query.*/
    while ($row = $fetch->fetch_array()) {

        $row_array['brg_kode'] 		    = $row['brg_kode'];
        $row_array['jenis_nama'] 		  = $row['jenis_nama'];
        $row_array['brg_merk'] 		    = $row['brg_merk'];
        $row_array['brg_type'] 		    = $row['brg_type'];
        $row_array['k_nama'] 		      = $row['k_nama'];

        array_push( $return_arr, $row_array );
    }


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
?>
