<?php

		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
		include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";
		
		/* getParam **/
		if($_GET['id']){
            $filter='WHERE brg_id='.$_GET['id'];
        }else{
            $filter='';
        }
		/* database **/
		try {
			$que 	= "SELECT * FROM v_lognikbarang ".$filter." order by time desc";
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
			$PLINK 	= null;
		}
		catch (PDOException $e){
			$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
		}

	echo json_encode($row);
    flush();
?>
