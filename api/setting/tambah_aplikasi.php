<?php
	session_start();
	error_reporting(0);
	date_default_timezone_set('Asia/Jakarta');
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/* getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai		= $_POST['data'];
	for($i=0;$i<count($nilai);$i++){
		define("__".$nilai[$i]['name'],$nilai[$i]['value']) ;
	}
$error="";
	try{
			$PLINK->beginTransaction();
			$que	= "INSERT INTO tm_aplikasi (`app_id`,`app_index`,`app_nama`,`app_exe`,`app_icon`,`app_menu`,`app_js`,`app`,`app_desc`,`app_class`) VALUES('".__app_id."','".__app_index."','".__app_nama."','".__app_exe."','".__app_icon."','".__app_menu."','".__app_js."','".__app."','".__app_desc."','".__app_class."')";

			if($PLINK->exec($que)>0){
				$title  = "Good Job!" ;
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "success";
				$url    = "103000" ;
			}
			else{
				$title  = "Sorry !" ;
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "warning";
				$url    = "103010" ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$title  = "Sorry !" ;
			$pesan	= "Data gagal disimpan";
			$kelas	= "error";
			$url    = "103010" ;
			$error	= $e->getMessage();
		}


	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que, "url" => $url,"title" => $title);
	echo json_encode($pesan);
?>
