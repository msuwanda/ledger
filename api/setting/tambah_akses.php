<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/** getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai	= $_POST['data'];

	for($i=0;$i<count($nilai);$i++){
		define("__".$nilai[$i]['name'],$nilai[$i]['value']) ;
	}
	/* getParam **/

	$error		= "";
	if(strlen(__grup_id)==3){
		try{
			$PLINK->beginTransaction();
			$que	= "INSERT INTO tr_access(app_id,grup_id) VALUES('".__app_id."','".__grup_id."')";
			if($PLINK->exec($que)>0){
				$title  = "Good Job!" ;
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "success";
				$url    = "true" ;
			}
			else{
				$title  = "Sorry !" ;
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "warning";
				$url    = "false" ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$title  = "Sorry !" ;
			$pesan	= "Data gagal disimpan";
			$kelas	= "error";
			$url    = "false" ;
			$error	= $e->getMessage();
		}
	}
	else{
		$title  = "Sorry !" ;
		$pesan	= "Permintaan tidak dapat diterima";
		$kelas	= "error";
		$url    = "false" ;
	}

	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que, "url" => $url,"title" => $title);
	echo json_encode($pesan);
?>
