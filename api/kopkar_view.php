<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
header('Access-Control-Allow-Origin: *');
/**
 * Script:    DataTables server-side script for PHP 5.2+ and MySQL 4.1+
 * Notes:     Based on a script by Allan Jardine that used the old PHP mysql_* functions.
 *            Rewritten to use the newer object oriented mysqli extension.
 * Copyright: 2010 - Allan Jardine (original script)
 *            2012 - Kari Söderholm, aka Haprog (updates)
 * License:   GPL v2 or BSD (3-point)
 */


/**
 * Array of database columns which should be read and sent back to DataTables. Use a space where
 * you want to insert a non-database field (for example a counter or static image)
 */
    $aColumns = array( 'z_register', 'z_nama', 'z_dev','z_bag','z_total','z_tahun', 'z_dev_1', 'z_bagian_1', 'z_total_1','z_keluar_1', 'z_status_1' , 'z_dev_2', 'z_bagian_2', 'z_total_2','z_keluar_2', 'z_status_2','z_dev_3', 'z_bagian_3', 'z_total_3','z_keluar_3', 'z_status_3','z_dev_4', 'z_bagian_4', 'z_total_4','z_keluar_4', 'z_status_4','z_dev_5', 'z_bagian_5', 'z_total_5','z_keluar_5', 'z_status_5','z_dev_6', 'z_bagian_6', 'z_total_6','z_keluar_6', 'z_status_6','z_dev_7', 'z_bagian_7', 'z_total_7','z_keluar_7', 'z_status_7','z_dev_8', 'z_bagian_8', 'z_total_8','z_keluar_8', 'z_status_8','z_dev_9', 'z_bagian_9', 'z_total_9','z_keluar_9', 'z_status_9','z_dev_10', 'z_bagian_10', 'z_total_10','z_keluar_10', 'z_status_10','z_dev_11', 'z_bagian_11', 'z_total_11','z_keluar_11', 'z_status_11','z_dev_12', 'z_bagian_12', 'z_total_12','z_keluar_12', 'z_status_12');

// Indexed column (used for fast and accurate table cardinality)
$sIndexColumn = 'z_register';

// DB table to use
$sTable = 'z_kopkar_saldo';

// Database connection information
include $_SERVER['DOCUMENT_ROOT']."/conf/setDBSS.php";

// Input method (use $_GET, $_POST or $_REQUEST)
$input =& $_GET;

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
 * no need to edit below this line
 */

/**
 * Character set to use for the MySQL connection.
 * MySQL will return all strings in this charset to PHP (if the data is stored correctly in the database).
 */
$gaSql['charset']  = 'utf8';

/**
 * MySQL connection
 */
$db = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db'], $gaSql['port']);
if (mysqli_connect_error()) {
    die( 'Error connecting to MySQL server (' . mysqli_connect_errno() .') '. mysqli_connect_error() );
}

if (!$db->set_charset($gaSql['charset'])) {
    die( 'Error loading character set "'.$gaSql['charset'].'": '.$db->error );
}


/**
 * Paging
 */
$sLimit = "";
if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
    $sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
}


/**
 * Ordering
 */
$aOrderingRules = array();
if ( isset( $input['iSortCol_0'] ) ) {
    $iSortingCols = intval( $input['iSortingCols'] );
    for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
        if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
            $aOrderingRules[] =
                "`".$aColumns[ intval( $input['iSortCol_'.$i] ) ]."` "
                .($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
        }
    }
}

if (!empty($aOrderingRules)) {
    $sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
} else {
    $sOrder = "";
}


/**
 * Filtering
 * NOTE this does not match the built-in DataTables filtering which does it
 * word by word on any field. It's possible to do here, but concerned about efficiency
 * on very large tables, and MySQL's regex functionality is very limited
 */
$iColumnCount = count($aColumns);

if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
    $aFilteringRules = array();
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
        if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
            $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string( $input['sSearch'] )."%'";
        }
    }
    if (!empty($aFilteringRules)) {
        $aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
    }
}

// Individual column filtering
for ( $i=3 ; $i<$iColumnCount ; $i++ ) {
    if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
        $aFilteringRules[] = "`".$aColumns[$i]."` LIKE '%".$db->real_escape_string($input['sSearch_'.$i])."%'";
    }
}

// filtering manual
if ( isset($input['bSearchable_0']) && $input['bSearchable_0'] == 'true' && $input['sSearch_0'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[0]."` LIKE '%".$db->real_escape_string($input['sSearch_0'])."%'";
    }
if ( isset($input['bSearchable_1']) && $input['bSearchable_1'] == 'true' && $input['sSearch_1'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[1]."` = '".$db->real_escape_string($input['sSearch_1'])."'";
    }
if ( isset($input['bSearchable_2']) && $input['bSearchable_2'] == 'true' && $input['sSearch_2'] != '' ) {
        $aFilteringRules[] = "`".$aColumns[2]."` = '".$db->real_escape_string($input['sSearch_2'])."'";
    }
// end filtering manual

if (!empty($aFilteringRules)) {
    $sWhere = " WHERE ".implode(" AND ", $aFilteringRules);
} else {
    $sWhere = "";
}


/**
 * SQL queries
 * Get data to display
 */
$aQueryColumns = array();
foreach ($aColumns as $col) {
    if ($col != ' ') {
        $aQueryColumns[] = $col;
    }
}

$sQuery = "
    SELECT SQL_CALC_FOUND_ROWS `".implode("`, `", $aQueryColumns)."`
    FROM `".$sTable."`".$sWhere.$sOrder.$sLimit;

$rResult = $db->query( $sQuery ) or die($db->error);

// Data set length after filtering
$sQuery = "SELECT FOUND_ROWS()";
$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

// Total data set length
$sQuery = "SELECT COUNT(`".$sIndexColumn."`) FROM `".$sTable."`";
$rResultTotal = $db->query( $sQuery ) or die($db->error);
list($iTotal) = $rResultTotal->fetch_row();


/**
 * Output
 */
$output = array(
    "sEcho"                => intval($input['sEcho']),
    "iTotalRecords"        => $iTotal,
    "iTotalDisplayRecords" => $iFilteredTotal,
    "aaData"               => array(),
);

while ( $aRow = $rResult->fetch_assoc() ) {
    $row = array();
    for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
        if ( $aColumns[$i] == 'version' ) {
            // Special output formatting for 'version' column
            $row[] = ($aRow[ $aColumns[$i] ]=='0') ? '-' : $aRow[ $aColumns[$i] ];
        } elseif ( $aColumns[$i] != ' ' ) {
            // General output
            $row[] = $aRow[ $aColumns[$i] ];
        }
    }
    //
    $smd_1   = ($aRow[ $aColumns[4]] + $aRow[ $aColumns[8]]) ;
    $bunga_1 = (($smd_1 * 0.016666) * $aRow[ $aColumns[9]]) ;
    $total_1   = ($smd_1 + $bunga_1) ;

    $row[] = number_format($smd_1,2) ;
    $row[] = number_format($bunga_1,2) ;
    $row[] = number_format($total_1,2) ;
    //if ($bunga_1>240000){ $pph_1 = ($bunga_1 * 0.1) ;}else {$pph_1 = 0 ;}
    $row[] = 0 ;


    $smd_2   = ($total_1 + $aRow[ $aColumns[18]]) ;
    $bunga_2 = (($smd_2 * 0.016666) * $aRow[ $aColumns[19]]);
    $total_2   = ($smd_2 + $bunga_2) ;

    $row[] = number_format($smd_2,2) ;
    $row[] = number_format($bunga_2,2) ;
    $row[] = number_format($total_2,2) ;
    //if ($bunga_1>240000){ $pph_2 = ($bunga_2 * 0.1) ;}else {$pph_2 = 0 ;}
    $row[] = 0 ;

    $smd_3   = ($total_2 + $aRow[ $aColumns[13]]) ;
    $bunga_3 = (($smd_3 * 0.016666) * $aRow[ $aColumns[14]]);
    $total_3   = ($smd_3 + $bunga_3) ;

    $row[] = number_format($smd_3,2) ;
    $row[] = number_format($bunga_3,2) ;
    $row[] = number_format($total_3,2) ;

    //if ($bunga_1>240000){ $pph_3 = ($bunga_3 * 0.1) ;}else {$pph_3 = 0 ;}
    $row[] = 0 ;

    $smd_4   = ($total_3 + $aRow[ $aColumns[23]]) ;
    $bunga_4 = (($smd_4 * 0.016666) * $aRow[ $aColumns[24]]);
    $total_4   = ($smd_4 + $bunga_4) ;

    $row[] = number_format($smd_4,2) ;
    $row[] = number_format($bunga_4,2) ;
    $row[] = number_format($total_4,2) ;

    ///if ($bunga_1>240000){ $pph_4 = ($bunga_4 * 0.1) ;}else {$pph_4 = 0 ;}
    $row[] = 0 ;

    $smd_5   = ($total_4 + $aRow[ $aColumns[28]]) ;
    $bunga_5 = (($smd_5 * 0.016666) * $aRow[ $aColumns[29]]);
    $total_5   = ($smd_5 + $bunga_5) ;

    $row[] = number_format($smd_5,2) ;
    $row[] = number_format($bunga_5,2) ;
    $row[] = number_format($total_5,2) ;

    //if ($bunga_1>240000){ $pph_5 = ($bunga_5 * 0.1) ;}else {$pph_5 = 0 ;}
    $row[] = 0 ;

    $smd_6   = ($total_5 + $aRow[ $aColumns[33]]) ;
    $bunga_6 = (($smd_6 * 0.016666) * $aRow[ $aColumns[34]]);
    $total_6   = ($smd_6 + $bunga_6) ;

    $row[] = number_format($smd_6,2) ;
    $row[] = number_format($bunga_6,2) ;
    $row[] = number_format($total_6,2) ;

    //if ($bunga_1>240000){ $pph_6 = ($bunga_6 * 0.1) ;}else {$pph_6 = 0 ;}
    $row[] = 0 ;

    $smd_7   = ($total_6 + $aRow[ $aColumns[38]]) ;
    $bunga_7 = (($smd_7 * 0.016666) * $aRow[ $aColumns[39]]);
    $total_7   = ($smd_7 + $bunga_7) ;

    $row[] = number_format($smd_7,2) ;
    $row[] = number_format($bunga_7,2) ;
    $row[] = number_format($total_7,2) ;

    //if ($bunga_1>240000){ $pph_7 = ($bunga_7 * 0.1) ;}else {$pph_7 = 0 ;}
    $row[] = 0 ;

    $smd_8   = ($total_7 + $aRow[ $aColumns[43]]) ;
    $bunga_8 = (($smd_8 * 0.016666) * $aRow[ $aColumns[44]]);
    $total_8   = ($smd_8 + $bunga_8) ;

    $row[] = number_format($smd_8,2) ;
    $row[] = number_format($bunga_8,2) ;
    $row[] = number_format($total_8,2) ;

    //if ($bunga_1>240000){ $pph_8 = ($bunga_8 * 0.1) ;}else {$pph_8 = 0 ;}
    $row[] = 0 ;

    $smd_9   = ($total_8 + $aRow[ $aColumns[48]]) ;
    $bunga_9 = (($smd_9 * 0.016666) * $aRow[ $aColumns[49]]);
    $total_9   = $smd_9 + $bunga_9 ;

    $row[] = number_format($smd_9,2) ;
    $row[] = number_format($bunga_9,2) ;
    $row[] = number_format($total_9,2) ;

    //if ($bunga_1>240000){ $pph_9 = ($bunga_9 * 0.1) ;}else {$pph_9 = 0 ;}
    $row[] = 0 ;

    $smd_10   = ($total_9 + $aRow[ $aColumns[53]]) ;
    $bunga_10 = (($smd_10 * 0.016666) * $aRow[ $aColumns[54]]);
    $total_10   = ($smd_10 + $bunga_10) ;

    $row[] = number_format($smd_10,2) ;
    $row[] = number_format($bunga_10,2) ;
    $row[] = number_format($total_10,2) ;

    //if ($bunga_1>240000){ $pph_10 = ($bunga_10 * 0.1) ;}else {$pph_10 = 0 ;}
    $row[] = 0 ;

    $smd_11   = ($total_10 + $aRow[ $aColumns[58]]) ;
    $bunga_11 = (($smd_11 * 0.016666) * $aRow[ $aColumns[59]]);
    $total_11   = ($smd_11 + $bunga_11) ;

    $row[] = number_format($smd_11,2) ;
    $row[] = number_format($bunga_11,2) ;
    $row[] = number_format($total_11,2) ;

    $row[] = 0 ;

    $smd_12   = ($total_11 + $aRow[ $aColumns[63]]) ;
    $bunga_12 = (($smd_12 * 0.016666) * $aRow[ $aColumns[64]]);
    $total_12   = $smd_12 + $bunga_12 ;

    $row[] = number_format($smd_12,2) ;
    $row[] = number_format($bunga_12,2) ;
    $row[] = number_format($total_12,2) ;

    //if ($bunga_1>240000){ $pph_12 = ($bunga_12 * 0.1) ;}else {$pph_12 = 0 ;}
    $row[] = 0 ;

    $row[] = number_format($total_1 + $total_2 + $total_3 + $total_4 + $total_5 + $total_6 + $total_7 + $total_8 + $total_9 + $total_10 + $total_11 + $total_12 ) ;
    $row[] = number_format($bunga_1 + $bunga_2 + $bunga_3 + $bunga_4 + $bunga_5 + $bunga_6 + $bunga_7 + $bunga_8 + $bunga_9 + $bunga_10 + $bunga_11 + $bunga_12 ) ;

    //$total = round($total + $aRow[ $aColumns[2]], 2);
    //$row[] = number_format($total, 2) ;
    $output['aaData'][] = $row;
}

echo json_encode( $output );
