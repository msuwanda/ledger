<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Origin: '.$_SERVER['REMOTE_ADDR']);

		include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

		/** getParam
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		if(isset($_POST['filter'])){
			$filter	= array();
			$nilai	= $_POST['filter'];
			for($i=0;$i<count($nilai);$i++){
				$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			}
			if($i>0){
				$filter	= "WHERE ".implode(' AND ',$filter);
			}
		}
		else{
			$filter	= "";
		}
		/* getParam **/

		/* database **/
		try {
			$que 	= "SELECT FORMAT(Sum( bb.bk_kredit ), 2) AS kredit,FORMAT(Sum( bb.bk_debet ), 2) AS debet,FORMAT((sup_saldo_awal + Sum( bb.bk_kredit ) - Sum( bb.bk_debet )),2) AS total,FORMAT(Sum(sp.sup_saldo_awal), 2) AS saldo_awal FROM tm_buku_bank AS bb INNER JOIN tr_supplier AS sp ON bb.bk_customer = sp.npwp WHERE ( bb.bak_akun = '5000' OR bb.bak_akun = '2110' ) AND sp.kln_id = 100202 AND bb.kln_id = 100202 AND bb.bk_tahun = 2018 AND sp.sup_tahun = 2018 AND bb.bk_tahun = 2018 " ;
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
			$PLINK 	= null;
		}
		catch (PDOException $e){
			$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
		}
	echo json_encode($row);
    flush();
?>
