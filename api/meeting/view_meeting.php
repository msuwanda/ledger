<?php
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Origin: *');

		include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

		/** getParam
			memindahkan semua nilai dalam array POST ke dalam
			variabel yang bersesuaian dengan masih kunci array
		*/
		if(isset($_POST['filter'])){
			$filter	= array();
			$nilai	= $_POST['filter'];
			for($i=0;$i<count($nilai);$i++){
				$filter[]	= $nilai[$i]['name']."='".$nilai[$i]['value']."'";
			}
			if($i>0){
				$filter	= "WHERE ".implode(' AND ',$filter);
			}
		}
		else{
			$filter	= "";
		}

		if(isset($_POST['short'])){
				$short="ORDER BY ".$_POST['short'][0]['name']." ".$_POST['short'][0]['value'];
		}
		else{
				$short	= "";
		}
		/* getParam **/

		/* database **/
		try {
			$que 	= "SELECT * FROM tm_meeting ".$filter."  ".$short;
			$sth 	= $PLINK->prepare($que);
			$sth->execute();
			$row	= $sth->fetchAll(PDO::FETCH_ASSOC);
			$PLINK 	= null;
		}
		catch (PDOException $e){
			$row    = array("pesan"=>"Inquiry data gagal dilakukan", "error"=>$e->getMessage(), "query"=>$que);
		}

	echo json_encode($row);
    flush();
?>
