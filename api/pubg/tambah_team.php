<?php
	session_start();
	error_reporting(0);
	date_default_timezone_set('Asia/Jakarta');
	header('Content-Type: text/event-stream');
	header('Cache-Control: no-cache');
	header('Access-Control-Allow-Origin: *');

	include $_SERVER['DOCUMENT_ROOT']."/conf/setDB02.php";

	/* getParam
		memindahkan semua nilai dalam array POST ke dalam
		variabel yang bersesuaian dengan masih kunci array
	*/
	$nilai		= $_POST['data'];
	for($i=0;$i<count($nilai);$i++){
		define("__".$nilai[$i]['name'],$nilai[$i]['value']) ;
	}
$error="";
	try{
			$PLINK->beginTransaction();
			$reg_id = __reg_id ;
		  $que	= "INSERT INTO reg_team(reg_id,team_nama,team_email,team_tlp,reg_tgl)
		          VALUES('".$reg_id."','".__team_nama."','".__team_email."','".__team_tlp."',NOW())";
		  $que2	= "INSERT INTO reg_crew(reg_id,crew_nama,crew_idgame,crew_no)
		          VALUES('".$reg_id."','".__ag_nama_1."','".__ag_idgame_1."','1'),('".$reg_id."','".__ag_nama_2."','".__ag_idgame_2."','2'),('".$reg_id."','".__ag_nama_3."','".__ag_idgame_3."','3'),
		          ('".$reg_id."','".__ag_nama_4."','".__ag_idgame_4."','4'),('".$reg_id."','".__ag_nama_5."','".__ag_idgame_5."','5')";

			if($PLINK->exec($que)>0){
				$PLINK->exec($que2) ;
				$title  = "Good Job!" ;
				$pesan 	= "Data telah berhasil disimpan";
				$kelas	= "success";
				$url    = "991000" ;
			}
			else{
				$title  = "Sorry !" ;
				$pesan 	= "Data tidak bisa disimpan";
				$kelas	= "warning";
				$url    = "991010" ;
			}
			$PLINK->commit();
		}
		catch(Exception $e){
			$PLINK->rollBack();
			$title  = "Sorry !" ;
			$pesan	= "Data gagal disimpan";
			$kelas	= "error";
			$url    = "991010" ;
			$error	= $e->getMessage();
		}


	$pesan  = array("pesan"=>$pesan, "kelas"=>$kelas, "error"=>$error, "query"=>$que, "url" => $url,"title" => $title);
	echo json_encode($pesan);
?>
